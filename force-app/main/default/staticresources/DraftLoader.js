/**
 * Name: DraftLoader.js 
 * Description: A controller that listens for the form AML.Ready event and looks for a draft object.
 *              The draft object is expected to be a JSON formatted string in state
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */

(function($)
{
    var BASE_CONFIG = 
    {
        draftStateProperty  : "",
        saveToStateProperty : "",
        dialogMessage       : "You have a pending draft, would you like to continue or discard the draft?",
        continueButtonLabel : "Continue",
        discardButtonLabel  : "Discard",
        employeeIdProperty  : "",
        controllerClass     : ""
    };

    function DraftLoader(context)
    {
        this._context = context;
        this.options = $.extend({}, BASE_CONFIG, context.options);
    }
    
    DraftLoader.prototype.execute = function(e, eventData, callback)
    {
        var _this = this;
        var draft = _this._context.local.state.get(_this.options.draftStateProperty);

        if(draft != null)
        {
            ACEConfirm.show
            (
                _this.options.dialogMessage,
                "",
                [ _this.options.continueButtonLabel +"::confirmButton", _this.options.discardButtonLabel + "::cancelButton"],
                function(result)
                {
                    if(result == _this.options.continueButtonLabel)
                    {
                        var items = {};
                        $.each(draft, function(index, data)
                        {
                            var draftItem = JSON.parse(data.Draft_JSON__c);
                            var externalId = draftItem.covRecord.Approval_External_Id__c;

                            items[externalId] = draftItem;
                        });
                        _this._context.local.state.put(_this.options.saveToStateProperty, items);
                    }
                    else if(result == _this.options.discardButtonLabel)
                    {
                        var empId = _this._context.local.state.get(_this.options.employeeIdProperty);
                        //deleteDraftsForEmployee
                        ACE.Remoting.call
                        (   _this.options.controllerClass + '.deleteDraftsForEmployee',
                            [empId],
                            function(result, event)
                            {
                                console.log('Deleted Draft for employee with Id: ' + empId);
                            }
                        );


                    }
                    callback(true);
                },
                {
                    "dialogClass" : "confirmationDialog aceDialog"
                }
            );
        }else
        {
            callback(true);
        }


    }

    ACE.AML.ControllerManager.register("extensions:draftLoader", DraftLoader);
//# sourceURL=DraftLoader.js
})(jQuery);