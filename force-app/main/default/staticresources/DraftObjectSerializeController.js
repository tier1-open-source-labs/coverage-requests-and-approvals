/**
 * Name: DraftObjectSerializeController.js 
 * Description: Controller that takes the data provider object and serializes it in JSON
 *              The JSON is then sent to the back end for archiving
 *
 * Confidential & Proprietary, 2020 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */

(function($)
{          
    var BASE_CONFIG = 
    { 
        dataProvider     : "coveragesMap",
        controllerMethod : "",
        employeeObj      : ""
    }

    function DraftObjectSerializeController(context)
    {        
        var _this = this;
        _this._context = context;        
        _this.options = $.extend({}, BASE_CONFIG, context.options);        
        _this._context.readyHandler();
    }
    
    DraftObjectSerializeController.prototype.execute = function(e, eventData, callback)
    {
        var _this = this;

        var data = _this._context.local.state.get(_this.options.dataProvider);
        var dataJSON = [];

        $.each(data, function(index, item)
        {
            dataJSON.push( JSON.stringify(item) );
        });

        var empId = _this._context.local.state.get(_this.options.employeeObj + '.Id');

        ACE.LoadingIndicator.show('Saving');//TODO Localize
        ACE.Remoting.call
        (
            _this.options.controllerMethod, 
            [empId, dataJSON],
            function(result, event)
            {
                ACE.LoadingIndicator.hide();
                if(event.status)
                {
                    callback(true);
                    ACE.LoadingIndicator.show('Save completed successfully');//TODO Localize
                    window.setTimeout(function()
                    {
                        ACE.LoadingIndicator.hide();
                    },2000);
                }
                else
                {
                    //specific message for overloaded drafts


                    ACEConfirm.show
                    (
                        event.message,
                        "Error",
                        [ "OK::confirmButton"],
                        function(result)
                        {
                            _this.close();
                        },
                        {
                            "dialogClass" : "confirmationDialog aceDialog"
                        }
                    );
                }
            }
        );

    }
   
    ACE.AML.ControllerManager.register("extensions:draftObjectSerializeController", DraftObjectSerializeController);    
//# sourceURL=DraftObjectSerializeController.js
})(jQuery);