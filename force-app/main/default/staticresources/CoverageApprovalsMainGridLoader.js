/**
 * Name: CoverageApprovalsMainGridLoader.js 
 * Description: Javascript Event Listener that listens for a Coverage Approval
 *              event and send the client center main grid selected items.
 *              This is to be loaded into the external resources AFR of Client Center
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */

(function($)
{
    ACE.CustomEventDispatcher.registerEventParams("coverageMainGridItemsReturn", function(e, args)
	{
		return args;
	});

    ACE.CustomEventDispatcher.on("coverageFetchMainGridItems", function(e, args)
    {
        console.log("Request Received, fetching main grid selected items");

        //CC 2.0+ has a flag that prevents events from triggering to protect against recursion
        //Set a timeout so that we execute when the event chain finishes. NOTE: I Hate this
        window.setTimeout(function(e, args){
            ACE.CustomEventDispatcher.trigger("coverageMainGridItemsReturn", 
            {
                "selectedItems" : ACE.ALM.modelLocator._mainPaneSelectedItems
            })
        },400
        );
    });
//# sourceURL=CoverageApprovalsMainGridLoader.js
})(jQuery);