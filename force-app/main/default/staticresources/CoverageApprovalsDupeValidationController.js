/**
 * Name: CoverageApprovalsDupeValidationController.js 
 * Description: Controller that validates entire grid data sets to check for dupes.
 *              It's pretty intensive so we will do operations in the back end. The 
 *              settings are all done via AFRs
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */

(function($)
{  

    var BASE_CONFIG = 
    {
        dataProvider : "coveragesMap",
        controller   : "CoverageApprovalsTriggerHelper",
        sanitizeFields : "invalid,_customIsSelected,id,T1C_Base__Account_Coverage__r.Approval_External_Id__c,errormsg,conflictingEmps"
    };  

    function CoverageApprovalsDupeValidationController(context)
    {
        this._context = context;	
        var __this = this;
        __this.options = $.extend({}, BASE_CONFIG, context.options);
        this.executionQueue = [];
        this.isRunning = false;
        this.ready=false;
        ACE.AML.Parser.APEXImporter.import('c__CoverageApprovalsTriggerHelperPage').then(function(e, args)
        {
            __this.ready = true;
            __this._context.readyHandler();

            if(__this.executionQueue.length > 0)
            {
                __this.isRunning = true;
                __this.executionQueue.shift().runQuery();
            }
        });
    }
    
    CoverageApprovalsDupeValidationController.prototype.execute = function(e, eventData, callback)
    {
        var __this = this;

        try
        {
            __this.validatedRowsMap = {};
            if(eventData.validatedRows)
            {
                $.each(eventData.validatedRows, function(index, item)
                {
                    __this.validatedRowsMap[item.value.id] = item;
                });
                
            }
            //Property change can still fire even if we're loading the Apex import
            if(!__this.isRunning || !this.ready)
            {
                __this.isRunning = true;
                __this.runQuery();
            }
            else
            {
                __this.executionQueue.push(__this);
            }
            callback(true);
        }
        catch(e)
        {
            console.log('Error in Dupe checker: ');
            console.log(e);
            callback(false);
            return false;
        }
    }
    
    CoverageApprovalsDupeValidationController.prototype.runQuery = function()
    {
        var __this = this;
        var validatedRows = {};
        var dataProvider = $.extend({},
            __this._context.local.state.get(__this.options.dataProvider));
        //Convert the data provider into a map using the row ID and the acai data.
        
        $.each(dataProvider, function(index, covWrapper)
        {
            //The data provider is always assumed to be a map of externalID and AC record
            //Fish out the ACAI records for processing
            var len = covWrapper.covAIs.length;
            for(var i = 0; i < len; i++)
            {
                var rowVal = $.extend({}, covWrapper.covAIs[i]);

                var delFields = __this.options.sanitizeFields.split(',');
                for(var x = 0; x < delFields.length; x++)
                {
                    delete rowVal[delFields[x]];
                }

                rowVal.T1C_Base__Account_Coverage__r = $.extend({}, covWrapper.covRecord);
                delete rowVal.T1C_Base__Account_Coverage__r.sObjectType;
                validatedRows[covWrapper.covAIs[i].id] = rowVal;
            }
        });

        if(Object.keys(validatedRows).length > 0)
        {
            ACE.Remoting.call(__this.options.controller + '.checkDupes',
            [validatedRows]
            ,function(result, event)
            {
                console.log('Dupe checker run finished');
                var validRows = [];
                //If we have invalid rows, create the error data and throw the event
                if(event.status && result && result.length > 0)
                {
                    //The result array will have the row Id and the message associated with that row
                    $.each(result, function(index, value)
                    {
                        //Massage the data a bit to look like the expected format of the grid controller
                        //The dupe checkers are always expected to return "gridId:message:conflicting employee name"
                        //conflicting employee name is optional
                        var data = value.split(':');
                        var row = 
                        {
                            id : data[0]
                        }
                        var msg = [data[1]]

                        if(__this.validatedRowsMap[row.id])
                        {
                            msg.concat(__this.validatedRowsMap[row.id].messages);                          
                        }

                        var confName = data[2] ? data[2] : null;

                        var error = {
                            "valid" : false,
                            "messages" : msg,
                            "value"    : row,
                            "conflictName" : confName
                        };
                        validRows.push(error);

                        delete validatedRows[row.id];
                    });
                }
                
                //Any other row that wasnt returned should be compared with the previous results
                $.each(validatedRows, function(index, item)
                {
                    var isValid = true;

                    var msg = [];

                    if(__this.validatedRowsMap[index])
                    {
                        msg = __this.validatedRowsMap[index].messages;
                        isValid = __this.validatedRowsMap[index].valid;
                    }

                    var validRow = {
                        "valid" : isValid,
                        "messages" : msg,
                        "value"    : item
                    };
                    validRow.value.id = index;

                    validRows.push(validRow);
                })

                var eventValidData = 
                {
                    "results" : 
                    {
                        "validatedRows" : validRows
                    }
                };

                __this._context.local.eventDispatcher.trigger(ACE.AML.Event.STATE_VALIDATION_CHANGE, eventValidData);

                if(__this.executionQueue.length < 1)
                {
                    __this.isRunning = false;
                }else
                {
                    __this.executionQueue.shift().runQuery();
                }
                
            });
        }
        else
        {
            __this.isRunning = false;
        }
    };

    ACE.AML.ControllerManager.register("extensions:coverageApprovalsDupeValidationController", CoverageApprovalsDupeValidationController);
//# sourceURL=CoverageApprovalsDupeValidationController.js
})(jQuery);