/**
 * Name: CoverageApprovalsRenotifyController.js 
 * Description: Controller that sends a re-notify request to the employee in state's approver
 *              This controller is Coverage Approvals-specific
 *
 * Confidential & Proprietary, 2020 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */

(function($)
{          
    var BASE_CONFIG = 
    { 
        employeeIdProperty : "",
        
        objType            : "T1C_Base__Account_Coverage_Additional_Info__c",
        successMessage     : "Email notification sent"
    }

    function CoverageApprovalsRenotifyController(context)
    {        
        var _this = this;
        _this._context = context;        
        _this.options = $.extend({}, BASE_CONFIG, context.options);        
        _this._context.readyHandler();
    }
    
    CoverageApprovalsRenotifyController.prototype.execute = function(e, eventData, callback)
    {
        var _this = this;

        var empId = _this._context.local.state.get(_this.options.employeeIdProperty);

        ACE.Remoting.call
        (
            "CoverageApprovalManagerController.renotifyApprover", 
            [empId, _this.options.objType],
            function(result, event)
            {
                if(result.status)
                {
                    ACE.LoadingIndicator.show(_this.options.successMessage);
                    window.setTimeout(function()
                    {
                        ACE.LoadingIndicator.hide();
                    },2000);

                    callback(true);
                }
                else
                {
                    ACEConfirm.show
                    (
                        result.message,
                        "Error",
                        [ "OK::confirmButton"],
                        function(select)
                        {
                            _this.close();
                        },
                        {
                            "dialogClass" : "confirmationDialog aceDialog"
                        }
                    );
                }
            }
        );

    }
   
    ACE.AML.ControllerManager.register("extensions:coverageApprovalsRenotifyController", CoverageApprovalsRenotifyController);    
//# sourceURL=CoverageApprovalsRenotifyController.js
})(jQuery);