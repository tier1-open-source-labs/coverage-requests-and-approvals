//=========================================================================================
/**
 * Name: CoverageApprovalsMultiPickList.js 
 * Description: Controller that renders a checkbox multiselect list for a given data provider in state
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */
//=========================================================================================
(function($)
{  

    /**
     * Controller that renders the state multi picklist control
     *
     * @constructor
     */
    function CoverageApprovalsMultiPickList(context)
    {
        var __this = this;
        __this._context = context;
        __this.options = context.options;
        
        this._context.readyHandler();
    }

    CoverageApprovalsMultiPickList.prototype.init = function()
    {
        var __this = this;
        var targetElement = $(__this._context.target);
        
        var innerText = __this.options.buttonPlaceHolder && __this.options.buttonPlaceHolder.length > 0 ? __this.options.buttonPlaceHolder : 'Select Items';
        //Render the dropdown control 
        __this._multiSelectRetracted = $('<button type="button" class="menuButton">' + innerText + '</button>').appendTo(targetElement);


        __this._createChildren(__this.options.dataProvider);
    };
    
    /**
     * @protected
     * 
     * Renders the dropdown component and initializes the listeners for the button.
     * The listeners were placed here so that in the event of an exception in the query,
     * the button still renders, but would be unresponsive
     *
     * @param {Array} dataProvider - A list of Objects in state to place in the checkbox list
     */
    CoverageApprovalsMultiPickList.prototype._createChildren = function(dataProvider)
    {
        var __this = this;
        var targetElement = $(__this._context.target);

        var options = 
        {
            'target'      : null,
            'dialogClass' : 'aceCheckboxListDialog',
            'modal'       : false
        };
        
        __this.multiSelectContainer = new ACE.AbstractCanvasDialog(options);
        
        __this.multiSelectContainer.position = function(value)
        {
            console.log('Reposition!');
            $(__this.multiSelectContainer.container).position(
            {
                'my':        "left top",
                'at':        "left bottom",
                'of':        $(targetElement),
                'collision': "fit"
            });
        };
        
        __this.dialogContainer = $(__this.multiSelectContainer.container);
        
        var defaultValues = __this.options.defaultValue ? __this.options.defaultValue : [];
        
        //__this.createFilterAndSelectAll(__this.dialogContainer);

        var provider = null;
        if( Array.isArray(dataProvider) )
        {
            provider = dataProvider;
        }
        else
        {
            return;
        }

        __this._renderPickList(provider, defaultValues, __this.dialogContainer);
        
        //Function for toggling the dropdown
        var toggleContainer = function(e, args)
        {
            //multiSelectContainer._showDialog();
            if(__this.multiSelectContainer.isOpen)
            {   
                __this.multiSelectContainer.close()
            } 
            else
            {
                //Add the document listener. If the user clicks outside of the dropdown then close it
                __this.multiSelectContainer._internalShow();
                //We don't want it to trigger the out of focus close
                e.stopImmediatePropagation();
                $(document).on('click scroll', outOfFocusClose);
            }
        };
        
        var outOfFocusClose = function(e, args)
        {
            //Clicking outside of the dropdown should close it
            if(__this.multiSelectContainer.isOpen && $(__this.dialogContainer).has(e.target).length === 0 )
            {
                __this.multiSelectContainer.close();
            }
        }
        
        //Whenever the dropdown closes, we commit to state. This handler operates on all close() calls for the abstract dialog
        __this.multiSelectContainer.beforeCloseHandler = function()
        {
            __this._context.local.state.put(__this.options.property, __this.__newCheckList.selectedItems);
            __this.updateLabel();
            return true;
        }
        
        __this._multiSelectRetracted.on('click',toggleContainer);
    };

    /**
     * @protected
     * 
     * Renders the ChecboxList component. Also prefills it on call report edits if the topics fall under the coverage list
     *
     * @param {Array} dataProvider - A list of items to display. Expects the standards {id, data, label} format
     *
     * @param {Array} preSelectedItems - A list of Ids to be selected by default.
     *
     * @param {Array} component - The element to attach the CheckboxList to
     */
    CoverageApprovalsMultiPickList.prototype._renderPickList = function(dataProvider, preSelectedItems, component)
    {
        var __this = this;
        var listOpts = 
        {
            'dataProvider' : dataProvider,
            'labelField'   : 'label'
        }
        
        var processList = [];
        $.each(dataProvider, function( index, item )
        {
            if( item.data && (item.data == __this.options.defaultValue || preSelectedItems.includes(item.data)) )
            {
                processList.push(item);
            }
        });
        
        
        __this.__newCheckList = new ACE.CheckboxList(listOpts);
        
        __this.__newCheckList.selectedItems = processList;
        
        __this.__newCheckList.appendTo(component);

        __this._context.local.state.put(__this.options.property, processList);

        __this.__newCheckList.on('click', function(e, args)
        {
            __this._context.local.state.put(__this.options.property, __this.__newCheckList.selectedItems);
            //__this.updateLabel();
        });
        
    };
    
    CoverageApprovalsMultiPickList.prototype.resetState = function(args, item)
    {
        var __this = this;
        __this._context.local.state.put(__this.options.property, __this.options.defaultValue);
        __this.updateLabel();
    }

    CoverageApprovalsMultiPickList.prototype.clearState = function(args, item)
    {
        var __this = this;
        __this._context.local.state.put(__this.options.property, null);
        __this.updateLabel();
    }

     /**
     * @protected
     * 
     * Renders the Filter and Select All Box
     *
     * @param {Array} component - The element to attach the CheckboxList to
     */
    CoverageApprovalsMultiPickList.prototype.createFilterAndSelectAll = function(component)
    {
        var filterParent = $('<div class="aceCheckboxList"></div>').appendTo(component);
        var filterList = $('<ul class="aceCheckboxList inner"></ul>').appendTo(filterParent);
        var filterListItem = $('<li class="aceMenuItem aceCheckboxListItem"></li>').appendTo(filterList);

        var filterContainer = $('<div class="gridFilterContainer" id="almMainGridFilterContainer"></div>').appendTo(filterListItem)
        var filter = $('<la-input id="almMainGridTextFilter" placeholder="Filter"/>').appendTo(filterContainer);

        var selectAllListItem = $('<li class="aceMenuItem aceCheckboxListItem"></li>').appendTo(filterList);
        var selectAll = $('<input type="checkbox" id="tickerSelectAll"></input>' )
                        .appendTo(selectAllListItem)
                        .checkbox();
        $('<span>Select All</span>').appendTo(selectAllListItem);

        $(selectAll).prop('isMaster', true);

        $(filter).keyup(function()
        {
            filterOptions(component, this);
        });

        $(filter).change(function()
        {
            filterOptions(component, this);
        });

        $(selectAll).click(function()
        {
            var checked = this.checked;
            var checkboxes = $(component).find('input');

            for(var x=0; x<checkboxes.length;x++)
            {
                var input = checkboxes[x];
                if(input.type == 'checkbox' && input.isHidden != true && input.isMaster != true)
                {
                    input.checked = checked;
                }
            }
        });
    }

    CoverageApprovalsMultiPickList.prototype.updateLabel = function()
    {
        //Display the selected items as a delimited list of names
        var __this = this;
        var displayString = '';

        if(__this.__newCheckList.selectedItems.length > 0)
        {
            $.each(__this.__newCheckList.selectedItems, function(index, item)
            {
                displayString += item.label + ', '
            });

            displayString = displayString.trim().slice(0, -1);
        }else
        {
            displayString = __this.options.buttonPlaceHolder ? __this.options.buttonPlaceHolder : 'Select Items';
        }

        __this._multiSelectRetracted.text(displayString);
        __this._multiSelectRetracted.attr('title', displayString);
    }


    function filterOptions(component, filter)
    {
        var filterValue = $(filter).val().toLowerCase();

        if(filterValue.length > 2)
        {
            var values = $(component).find('.aceCheckboxListItemLabel');

            for(var x=0;x<values.length;x++)
            {
                var valueLabel = values[x];
                var val = $(valueLabel).text().toLowerCase();

                if(val.indexOf(filterValue) !== -1)
                {
                    $(valueLabel.parentElement).show();
                    var input = $(valueLabel.parentElement).find('input');
                    $(input).prop('isHidden', false);
                }
                else
                {
                    $(valueLabel.parentElement).hide();
                    var input = $(valueLabel.parentElement).find('input');
                    $(input).prop('isHidden', true);
                }
            }
        }
        else if(filterValue.length == 0)// show all
        {
            var values = $(component).find('.aceCheckboxListItemLabel');
            
            for(var x=0; x<values.length;x++)
            {
                var valueLabel = values[x];

                $(valueLabel.parentElement).show();
                var input = $(valueLabel.parentElement).find('input');
                $(input).prop('isHidden', false);
            }
        }
        else // hide all
        {
            var values = $(component).find('.aceCheckboxListItemLabel');
            
            for(var x=0; x<values.length;x++)
            {
                var valueLabel = values[x];

                $(valueLabel.parentElement).hide();
                var input = $(valueLabel.parentElement).find('input');
                $(input).prop('isHidden', true);  
            }
        }
    }

    ACE.AML.ControllerManager.register("extensions:coverageApprovalsMultiPickList", CoverageApprovalsMultiPickList);
    
//# sourceURL=CoverageApprovalsMultiPickList.js
})(jQuery);