/**
 * Name: ExtensionsAMLLoader.js 
 * Description: Short javascript that tells the window to import
 *              an Ellipsis layout into the form
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */

(function($)
{
    var path            = ACEUtil.getURLParameter('layoutPath');
    var controllerClass = ACEUtil.getURLParameter('apexClass');
    var viewName        = ACEUtil.getURLParameter('apexParams');
    var userId          = ACEUtil.getURLParameter('userId');
    var accountId       = ACEUtil.getURLParameter('accountId');
    var source          = ACEUtil.getURLParameter('source');
    //Make sure that accountId is null if there's no value
    accountId = accountId && accountId.trim().length > 0 ? accountId : null; 
    console.log('AML test: ' + path);

    var accountIds = accountId ? [accountId] : [];

    var _this = this;
    //This event only gets triggered when the source is from CC.
    ACE.CustomEventDispatcher.on("coverageMainGridItemsReturn",function(e, args)
    {
        console.log("Confirming Grid event from CC");
        //Load the selectedItems into state
        _this.load.getLocalScope().state.put('selectedMainGridItems', args.selectedItems);
    });

    var showError = function(result, event)
    {
        ACE.FaultHandlerDialog.show
        ({
            title:   "Error",
            message: event.message, 
            error:   event.message
        });
    }

    var findInState = function(array, value)
    {
        var hasMatch = false;
        var arrlength = array.length;
        for(var i = 0; i < arrlength; i++ )
        {
            if(array[i].data == value )
            {
                hasMatch = true;
                break;
            }
        }

        return hasMatch;
    }


    if(ACE.AML)
    {
        ACE.AML.Parser.APEXImporter.import(ACE.Salesforce.baseURL + '/apex/CoverageApprovalManagerControllerPage').then(function(e,args)
        {
            ACE.Remoting.call
            (   controllerClass + '.getState',
                [viewName, userId, accountIds],
                function(result, event)
                {
                    if(!event.status)
                    {
                        showError(result, event);
                        return;
                    }
                    var state = result;
                    console.log('Ready to Load');
                    
                    //Check the results for the default employee and product/region lists
                    //By default we look at the T1_Region__c field on the employee object
                    if( state.employeeRegions && Array.isArray(state.employeeRegions) )
                    {
                        if(!findInState(state.employeeRegions, state.employee.T1_Region__c))
                        {
                            state.employee.T1_Region__c = null;
                        }
                    }

                    //Check the results for the default employee and product/region lists
                    //By default we look at the T1_Region__c field on the employee object
                    if( state.employeeProducts && Array.isArray(state.employeeProducts) )
                    {
                        if(!findInState(state.employeeProducts, state.employee.T1C_Base__Product_Lookup__c))
                        {
                            state.employee.T1C_Base__Product_Lookup__c = null;
                        }
                    }


                    if(source == 'clientCenter')
                    {
                        ACE.CustomEventDispatcher.trigger("coverageFetchMainGridItems");
                        _this.load = ACE.AML.DOMUtil.load("{@" + path + ".Layout}", "body", state, false, null);
                    }
                    else
                    {
                        _this.load = ACE.AML.DOMUtil.load("{@" + path + ".Layout}", "body", state, false, null);
                    }
                },
                //I placed showFaultHandler = false in the additionalOptions as well just to be sure
                {
                    showFaultHandler : false
                },
                {
                    showFaultHandler : false
                }
            );
        });
    }
//# sourceURL=ExtensionsAMLLoader.js
})(jQuery);