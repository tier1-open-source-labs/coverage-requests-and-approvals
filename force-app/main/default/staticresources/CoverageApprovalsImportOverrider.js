/**
 * Name: CoverageApprovalsImportOverrider.js 
 * Description: Overrides the assignment that the AML library import does to the core wrapper import.
 *              DO NOT USE THIS. This is just a temporary workaround until we get a formal
 *              Ellipsis window.
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */

(function($)
{
    var origDispatcher = ACE.CustomEventDispatcher;
    Object.defineProperty(ACE, "CustomEventDispatcher", 
    {
        get : function()
        {
            return origDispatcher;
        },
        set : function(obj)
        {
            if(!origDispatcher || origDispatcher == null)
            {
                origDispatcher = obj;
            }
        }
    });

//# sourceURL=CoverageApprovalsImportOverrider.js
})(jQuery);