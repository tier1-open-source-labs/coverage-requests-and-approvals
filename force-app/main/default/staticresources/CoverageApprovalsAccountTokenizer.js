/**
 * Name: CoverageApprovalsAccountTokenizer.js
 * Description: Grid controller with sub grid. Parent rows are done using group by
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */

(function($)
{
    var COMPONENT_HTML = '<div id="_container">' +
                            '<div id="_searchSectionWrapper" class="searchSectionWrapper el-row flex-parent is-horizontal">' +
                                '<div id="_searchInputWrapper" class="searchInputWrapper el-item-wrapper flex-parent is-vertical">' +
                                    '<div id="_searchInputContainer" class="searchInputContainer el-item flex-parent is-vertical"></div>' +								
                                '</div>'+
                                '<div id="_checkBoxContainer" class="clientHierarchyBox el-checkbox-wrapper">' +
                                    '<p class="el-warningMessage" style="display:none"></p>' +
                                '</div>' +
                                //this needs to throw the addNewToGrid event
                                '<button id="_addNewButton" class="el-btn-outline el-btn-med"></button>' +
                            '</div>' +								
                            '<div id="_tokenContainer" class="tokensContainer flex-parent is-horizontal"></div>' +
                        '</div>';
    
    var BASE_CONFIG = 
    {
        lookupConfig : 
        {
			additionalQueryFields : [],
			additionalWhereClause : null,
            selectedItemFormat    : "{Name}",
            mode                  :	"ACCOUNTS",
            coverageOnly          : false,
            maintainSelectedItem  :	false,
            hoverText             : "Type an Account name to search"
        },
        //event to listen for when adding is done
        addAccountEvent : 'addNewToGridFinished',
        stateProperty : 'selectedAccounts',
        enableClientHierarchy : true,
        clientHierarchyLabel  : "Include entire client hierarchy",
        addNewButtonText      : "Add New Client"
    }
    
    function CoverageApprovalsAccountTokenizer(context)
    {
        this._context = context;
        this.options = $.extend(true, BASE_CONFIG, context.options);
    }
    
    CoverageApprovalsAccountTokenizer.prototype.init = function(callback)
    {
        var _this = this;
        _this.accountsList = [];
        _this.selectedAccounts = {};
        this.uiComponent = new ACE.AML.UIComponent(this._context, COMPONENT_HTML, true);
        
        //First create the AutoComplete
        var inputContainer = $p('<LA-INPUT title="' + _this.options.hoverText + '"></LA-INPUT>')
            .appendTo($(_this.uiComponent._searchInputContainer));
        
        var autoComplete = new SearchAutoComplete($(inputContainer), _this.options.lookupConfig);
        
        

        autoComplete.set("select", function(item)
        {
            //When we get an account, check if the Client hierarchy checkbox is toggled
            if(_this.addClientHierarchy)
            {
                //Match the lookupConfig on the autocomplete
                var additionalFields = _this.options.lookupConfig && _this.options.lookupConfig.additionalQueryFields ?
                    _this.options.lookupConfig.additionalQueryFields : [];

                console.log('Selecting client hierarchy');
                ACE.Remoting.call('CoverageApprovalManagerController.getClientHierarchy',
                    [item.Id, additionalFields],
                    function(result, eventData)
                    {
                        console.log('Ready to Load');
                        $.each(result, function(index, row)
                        {
                            //Check if the account is already in the selected accounts
                            if(_this.selectedAccounts[row.Id] == null)
                            {
                                _this.selectedAccounts[row.Id] = row;
                                _this.renderToken(row);
                                
                            }
                        });

                        _this._context.local.state.put(_this.options.stateProperty, _this.selectedAccounts);
                    });
                //Call the select class here
            }
            else if(!_this.selectedAccounts[item.Id])
            {
                _this.selectedAccounts[item.Id] = item;
                //Render selected account tokens
                _this.renderToken(item);
                _this._context.local.state.put(_this.options.stateProperty, _this.selectedAccounts);
            }
            
        });

        //Then create the Client Hierarchy checkbox
        if(_this.options.enableClientHierarchy)
        {
            var checkInput = $('<input type="checkbox" class="el-checkbox-input"></input>').prependTo(this.uiComponent._checkBoxContainer);

            var opts =
            {
                "defaultValue": false,
                "label": _this.options.clientHierarchyLabel,
                "labelPosition": "right",
                "labelCssClass" : "el-checkbox-label",
			    "styleName" : "el-checkbox"
            };
            checkInput.checkbox(opts);

            checkInput.on('click', function(e, args)
            {
                _this.addClientHierarchy = $(checkInput)[0].checked;
            });
        }
        _this.uiComponent._tokenContainer.click(function(e) 
        {
            var target = $(e.target);
            var dataType = target.attr("data-type");
            var itemId = target.attr("account-id");
            if(dataType == "deleteItem")
            {
                delete _this.selectedAccounts[itemId];
                //Remove the token
                $(target).parent().remove();
                _this._context.local.state.put(_this.options.stateProperty, _this.selectedAccounts);
            }								       		
        });

        $(_this.uiComponent._addNewButton).on("click", function(e, args)
        {
            //TODO Make configurable
            _this._context.local.eventDispatcher.trigger("addNewToGrid");
        })

        $(_this.uiComponent._addNewButton).text(_this.options.addNewButtonText);

        _this._context.readyHandler();
    }
    
    CoverageApprovalsAccountTokenizer.prototype.setOption = function(name, val)
    {
        var _this = this;
        //If our data provider in state changes, we should wipe the tokens and render the new ones
        if(name == "dataProvider" && !val )
        {
            var parent = $(this.uiComponent._tokenContainer);
            $(parent).empty()
            _this.selectedAccounts = {};
        }
    };

    CoverageApprovalsAccountTokenizer.prototype.renderToken = function(accountData)
    {
        var _this = this;
        var tokenHtml = '<div class="autoCompleteToken">'+
                            '<span id="' + accountData.Id + '" class="autoCompleteTokenLabel">' + accountData.Name + '</span>' +
                            '<span class="autoCompleteTokenDeleteButton" account-id="' + accountData.Id + '" data-type="deleteItem">X</span>' +
                        '</div>';
        
        var tokenField = $(tokenHtml);
        tokenField.appendTo(this.uiComponent._tokenContainer);
    };

    ACE.AML.ControllerManager.register("extensions:coverageApprovalsAccountTokenizer", CoverageApprovalsAccountTokenizer);
	//# sourceURL=CoverageApprovalsAccountTokenizer.js
})(jQuery);
