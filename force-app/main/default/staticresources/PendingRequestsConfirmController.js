/**
 * Name: PendingRequestsConfirmController.js 
 * Description: 
 *
 * Confidential & Proprietary, 2020 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */

(function($)
{          
    var BASE_CONFIG = 
    { 
        selectedRequestsProperty : "selectedRequestsCount",
        //optionalTextProperty     : "selectedRequestsReasons",
        textBoxLabel             : "Reason (optional)",
        dialogLayoutPath         : "",
        action                   : "Approve",
        message                  : "You have selected {selectedRequestsCount} requests to approve. Do you wish to proceed?",
        coveragesToSendProperty  : "coveragesMap",
        gridSelectedProperty     : "_customIsSelected",
        employeeProperty         : "employee.T1C_Base__User__c",
        showTextArea             : false,
        batchSize                : 25
    }

    function PendingRequestsConfirmController(context)
    {        
        var _this = this;
        _this._context = context;        
        _this.options = $.extend({}, BASE_CONFIG, context.options);        
        _this._context.readyHandler();
    }
    
    PendingRequestsConfirmController.prototype.execute = function(e, eventData, callback)
    {
        var _this = this;
        
        //Need to preserve the call back reference since it will get lost after the dialog calls sendRecordsForProcessing()
        _this.callback = callback;

        //A simple latch to make sure we dont submit multiple times while already saving
        if( _this._context.local.state.get('isSaving') )
        {
            callback(false);
            return;
        }

        _this._context.local.state.put('isSaving', true);

        var numRequests = _this._context.local.state.get(_this.options.selectedRequestsProperty);
        var title = ACE.StringUtil.mergeParameters(_this.options.message, _this._context.local.state.data);
        //Create an AML dialog
        _this._context.global.eventDispatcher.trigger(ACE.AML.Event.SHOW_DIALOG,
        {
            "configPath": _this.options.dialogLayoutPath,
            "isCanvas": false,
            //"title"   : title,
            "state":
            { 
                "numRequests"   : numRequests,
                //We also pass this controller to the dialog state so we can call it on confirm
                "parentControl" : _this,
                "showTextArea"  : _this.options.showTextArea,
                "warningMsg"    : title,
                "reasonText"    : "",
                "textBoxLabel"  : _this.options.textBoxLabel
            }
        });

        _this._context.local.state.put('isSaving', false);
    }
   
    PendingRequestsConfirmController.prototype.sendRecordsForProcessing = function(dialogState)
    {
        var _this = this;
        console.log( 'Sending records to ' + _this.options.action );

        var covParents = _this._context.local.state.get(_this.options.coveragesToSendProperty);
        var covIds = [];

        $.each(covParents, function(key, covs)
        {
            $.each(covs, function(index, item)
            {
                if(item[_this.options.gridSelectedProperty])
                {
                    covIds.push(item.id);
                }
            });
        });
        
        var userId = _this._context.local.state.get(_this.options.employeeProperty);
        var reasonText = dialogState.reasonText;
        
        var controllerClass = 'CoverageApprovalManagerController';
        //SaveResult processRecords( List<Id> approvedObjs, String action, Id statusChangedUserId, String actionReason )

        var batch = parseInt(_this.options.batchSize);
        batch = batch > 0 ? batch : 25;

        /*ACE.Remoting.call
        (   controllerClass + '.processRecords',
            [covIds, _this.options.action, userId, reasonText],*/
        ACE.Remoting.batch(controllerClass + '.processRecords', covIds, function(args)
        {
            return [args, _this.options.action, userId, reasonText];
        },
        function(result, event)
        {
            _this._context.local.state.put('isSaving', false);
            if(event.status)
            {
                _this._context.local.state.put('selectedRequestsCount', 0);
                _this._context.local.eventDispatcher.trigger('coverageFinishedSaving', {"covAIIds": covIds});
            }
        },
        {
            retryOnTimeout : false,
            batchSize : batch,
            showProgressBar : true,
            alwaysShowProgressBar : true
        },
        {
            retryOnTimeout : false,
            batchSize : batch,
            showProgressBar : true,
            alwaysShowProgressBar : true
        });   

        _this.callback(true);
    }

    ACE.AML.ControllerManager.register("extensions:pendingRequestsConfirmController", PendingRequestsConfirmController);    
//# sourceURL=PendingRequestsConfirmController.js
})(jQuery);