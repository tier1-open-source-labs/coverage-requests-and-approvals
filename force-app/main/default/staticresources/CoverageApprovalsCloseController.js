/**
 * Name: CoverageApprovalsCloseController.js
 * Description: Grid controller with sub grid. Parent rows are done using group by
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */

(function($)
{
    var BASE_CONFIG = 
    {
        "unsavedChangesMessage" : "You have unsaved changes. Are you sure you want to quit?",
        "changesProperty"       : "footerCoveragesCount"
    }
    
    function CoverageApprovalsCloseController(context)
    {
        this._context = context;
        this.options = $.extend(BASE_CONFIG, context.options);
    }
    
    CoverageApprovalsCloseController.prototype.execute = function(callback)
    {
        //If there are changes, pop the confirm
        var numCoverages = this._context.local.state.get(this.options.changesProperty);
        if(numCoverages && numCoverages > 0)
        {
            //TODO: Ace Confirm here
            
            window.close();
        }
        else
        {
            window.close();
        }
    }
    
    ACE.AML.ControllerManager.register("extensions:coverageApprovalsCloseController", CoverageApprovalsCloseController);
	//# sourceURL=CoverageApprovalsCloseController.js
})(jQuery);