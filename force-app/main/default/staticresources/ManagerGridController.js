/**
 * Name: ManagerGridController.js
 * Description: Grid controller with sub grid. Parent rows are done using group by
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */

(function($)
{
    var BASE_CONFIG =
    {
        showCheckboxes      : true,
        //The propery in state which stores the selected account Objects on the form
        addAccountProperty  : "selectedAccounts",
        addAccountEvent     : "addNewToGrid",
        bulkUpdateEvent     : "performBulkOperation",
        //Data Provider from state
        dataProvider        : null,
        editable            : true,
        setGroupByEvent     : "requestGridSetGrouping",
        //employeeObj: The state property to get employee from. Used for generating external IDs for AC records along with account
        employeeObj         : "employee",
        acExternalIdField   : "Approval_External_Id__c",//The AC object's external Id field,
        coverageType        : "T1C_Base__Account_Coverage__c",
        additionalInfoType  : "T1C_Base__Account_Coverage_Additional_Info__c",
        //State property to insert the map into
        saveToStateProperty : "coveragesMap",
        stateDraftProperty  : "",
        //Footer state values and default text
        //footerAccountsCount  : "",
        //footerCoveragesCount : "",
        footerAccountPropertyName : "pendingAccountsCount",
        footerCoveragePropertyName : "pendingAIsCount",
        footerText : "{footerAccountsCount} Accounts, {footerCoveragesCount} Coverage Requests",
        //Group By enabled
        gridType             : "Request",
        enableGrouping       : false,
        //The field to place the parent object on (Additional Info records are master details)
        //This gets deleted before saving
        parentObjectField    : "T1C_Base__Account_Coverage__r",
        //The default parent field to group by NOTE: this is on the AC object
        groupByField         : "Approval_External_Id__c",
        groupByLabelField    : "T1C_Base__Account__r.Name",
        //The propery in state which stores the selected requests count on the form
        selectedRequestsCountProperty : "selectedRequestsCount",
        //Default grid mode for bulk coverage modifications
        defaultGridMode      : "Add",
        //Field map which states which values in state go into the grid rows during update/add
        bulkOperationFieldMap : {},
        textFilter: "",
        //Needs to be configurable since multiple grids use this controller
        groupByFilterProperty : "",
        durationFilter : "",
        durationFilterProperty : "gridFilterDuration",
        durationField : "Status_Changed_Date__c",
        statusFilter : "",
        statusFilterProperty : "gridFilterStatus",
        statusField : "Approval_Status__c",
        enableCancelRequestColumn : false,
        stateFiltersEnabled : false,
        cancelRequestLabel : "Cancel Request",
        cancelRequestMessage : "Cancel this request?"
    }

    //var TRASH_ICON = '<div class="toggle delete"></div>';

    function ManagerGridController(context)
    {
        this._context = context;
        this.options = $.extend(true, {}, BASE_CONFIG, context.options);

        //The product mandatory arrays had to be converted to strings in order for locales to work properly
        //Handle the array input anyway in case we get it
        if(this._context.options.productMandatoryRoles != null && !Array.isArray(this._context.options.productMandatoryRoles))
        {
            this._context.options.productMandatoryRoles = this._context.options.productMandatoryRoles.split(',');
            $.each(this._context.options.productMandatoryRoles, function(index, item)
            {
                item = item.trim();
            });
        }

        context.readyHandler();
    }

    ManagerGridController.prototype.init = function()
    {
        var _this = this;
        this.groupByFormatterMap =
        {
            //The requester grid so far only supports 1 level
            "Request" : function (aggregate)
            {
                var covWrapper = _this.coveragesMap[aggregate.value];
                var param = '{' + _this.options.groupByLabelField + '} (' + aggregate.count + ')';
                var groupByLabel = ACE.StringUtil.mergeParameters(param, covWrapper.covRecord);
                var checkBox = "<div class='flex-parent is-horizontal'><input ";

                if(covWrapper.selected)
                {
                    checkBox += " checked='checked' ";
                }

                if(_this.gridMode.toLowerCase() == 'update')
                {
                    checkBox += " disabled "
                }

                //TODO remove hard coded width/height
                checkBox += "data='groupByCheckBox' class='groupByCheckBox' type='checkBox'></input>" + groupByLabel + 
                    "<div style='width:30px;height:22px;' data='addRow' class='plusIcon'></div>" + 
                    "<div style='width:30px;height:22px;' data='deleteButton' class='deleteButton'></div></div>";
                return  checkBox;
            },
            "Approver" : function (aggregate)
            {
                //In the case of approver view, since we're not saving the ACAI records, the coverages map just has the list of ACAI objects,
                //not the coverage wrapper object. So it is OK to grab the first row in the aggregate and grab the field value from it
                var covRecord = aggregate.rows[0];
                var groupByField = _this.options.parentObjectField + '.' + _this.options.groupByLabelField;
                var param = '{' + groupByField + '} (' + aggregate.count + ')';
                var groupByLabel = ACE.StringUtil.mergeParameters(param, covRecord);

                var selected = true;
                //Check the children, if they're all checked, then we must also check this parent
                //Only do this if the aggregate isnt checked
                for(var a = 0; a < aggregate.rows.length && selected && !aggregate.selected; a++)
                {
                    selected = selected && aggregate.rows[a]._customIsSelected;
                }

                var checkBox = '';
                if(selected || aggregate.selected)
                {
                    checkBox += " checked='checked' ";
                    aggregate.selected = true;
                }

                if(_this.options.showCheckboxes)
                {
                    //We use external ID for the account records, but the other lookups use the __r relationships
                    if(aggregate.level > 0)
                    {
                        //The &emsp; is to create a tab-sized space to show checkbox levels
                        return "<span>&emsp;<input data='groupByCheckBox'" + checkBox + " class='groupByCheckBox' type='checkBox'></input>" + aggregate.value + "</span>";
                    }

                    return "<span><input data='groupByCheckBox'" + checkBox + " class='groupByCheckBox' type='checkBox'></input>" + groupByLabel + "</span>";
                }
                else
                {
                    //We use external ID for the account records, but the other lookups use the __r relationships
                    if(aggregate.level > 0)
                    {
                        //The &emsp; is to create a tab-sized space to show checkbox levels
                        return "<span>&emsp;" + aggregate.value + "</span>";
                    }
                    else
                    {
                        return "<span>" + groupByLabel + "</span>";
                    }
                }
            }            
        };

        //Index for new rows
        this.newRowIndex = 0;
        this.coveragesMap = {};
        this.empObj = this._context.local.state.get(this.options.employeeObj);

        //Creating a grid container so the footer gets appeneded to the target and not the grid
        //  itself which cause the footer to disappear
        var gridContainer = $('<div></div>').appendTo(_this._context.target);

        this._context.local.eventDispatcher.on(ACE.AML.Event.READY, function(e, args){
            //The grid columns are already in state
            var gridCols = []
            _this.gridMode = _this.options.defaultGridMode;
            if(_this.options.gridType == 'Request')
            {
                _this._context.local.state.put('bulkCoverageMode',_this.gridMode);
            }
            

            //Checks for grid mode, if the grid is in Update mode,
            //we should be able to check the normal rows. This function does not run for the groupby rows
            var checkBoxValidator = function(item)
            {
                var isUpMode = _this.gridMode.toLowerCase() == 'update';
                console.log('Disable row checks? ' + isUpMode);
                
                return isUpMode;
            }

            if(_this.options.showCheckboxes)
            {
                gridCols.push({
                    id : "checkBoxRow",
                    width : 30,
                    field : "selected",
                    visible : true,
                    formatter : "CheckBoxSelectorItemRenderer",
                    additionalOptions :
                    {
                        checkBoxSelectorValidationFunction : checkBoxValidator
                    },
                    order: 0
                });
            }

            gridCols = gridCols.concat( _this.options.columns );

            /*if(_this.options.validationEnabled)
            {
                gridCols.push({
                    id : "errorCol",
                    width : 30,
                    field : "invalid",
                    visible : true,
                    formatter : "errorRenderer",
                    order: 9
                });
            }*/

            if(_this.options.enableCancelRequestColumn)
            {
                gridCols.push({
                    id : "cancelRequestCol",
                    minWidth : 120,
                    field : "",
                    name : _this.options.cancelRequestLabel,
                    visible : true,
                    formatter : "cancelRequestButton",
                    order: 9
                });
            }

            _this.addFormattersAndEditors(gridCols);

            _this.reqGrid = new ACEDataGrid( gridContainer,
            {
                explicitInitialization: true,
                sortNullsLast: true,
                sortable: true,
                autoEdit: false,
                editOnClick :true,
                forceFitColumns: true,
                multiSelect: true,
                pinGroupByToLeft: false,
                editable: _this.options.editable,
                enableGroupBy : _this.options.enableGrouping,
                allowRowSelection : true,
                checkBoxSelectorValidationFunction : checkBoxValidator,
                textFilter : $(_this.options.textFilter).find('input'),
                itemRendererFilterFunctions : 
                {
                    "statePicklist" : function(item, textFilter)
                    {
                        //_this.pickListFilterMap => Stores the Id and Label for every picklist data provider
                        //_this.statePicklistsFields => Stores the fields which contain lookups

                        var isMatch = false;
                        for(var i = 0; i < _this.statePicklistsFields.length; i++)
                        {
                            var id = item[_this.statePicklistsFields[i]];
                            var val = _this.pickListFilterMap[id] ? _this.pickListFilterMap[id] : '';
                            isMatch = val.match(textFilter);
                            if(isMatch)
                            {
                                break;
                            }
                        }
                        
                        return isMatch;
                    },
                    //HACK -> Account name is a hard coded default since it's required by the groupbys. We just need a renderer to use as a custom filter
                    "cancelRequestButton" : function(item, textFilter)
                    {
                        var isMatch = false;

                        var groupByField = _this.options.parentObjectField + '.' + _this.options.groupByLabelField;
                        var param = '{' + groupByField + '}';
                        var val = ACE.StringUtil.mergeParameters(param, item);

                        var match = val.match(textFilter);
                        isMatch = match && match.length > 0;

                        return isMatch;
                    },
                    "CheckBoxSelectorItemRenderer" : function(item, textFilter)
                    {
                        var isMatch = false;

                        var groupByField = _this.options.parentObjectField + '.' + _this.options.groupByLabelField;
                        var param = '{' + groupByField + '}';
                        var val = ACE.StringUtil.mergeParameters(param, item);

                        var match = val.match(textFilter);
                        isMatch = match && match.length > 0;

                        return isMatch;
                    }
                }
            });

            //quick override on the updateCheckBoxSelection since we now also have to take the groupby
            //rows into account
            _this.reqGrid.superUpdateCheckBoxSelection = _this.reqGrid.updateCheckBoxSelection;
            _this.reqGrid.updateCheckBoxSelection = _this.options.gridType == "Request" ? 
                _this.requestModeCheckBoxUpdater() : _this.approveModeCheckBoxUpdater();

            //override _updateSelectAllCheckBox with 'add' logic as well

            _this.reqGrid.setColumns( gridCols,
            {
                "statePicklist" : new ItemRenderer(function(row, cell, value, columnDef, dataContext)
                {
                    //Need to go back to the field in state and match the data Field for a name
                    var stateList = _this._context.local.state.get(columnDef.stateProvider);

                    var nameField  = columnDef.labelField;
                    var matchField = columnDef.listDataField;

                    var nameVal = "";
                    if(stateList)
                    {
                        $.each(stateList, function(index, item)
                        {
                            if(item[matchField] == value && value.length > 0)
                            {
                                nameVal = item[nameField];
                                return false;
                            }
                        });
                    }

                    return nameVal;
                }),
                "deleteButton" : new ItemRenderer(function(row, cell, value, columnDef, dataContext)
                {
                    return '<div data="deleteButton" class="deleteButton"></div>';
                }),
                "errorRenderer" : new ItemRenderer(function(row, cell, value, columnDef, dataContext)
                {
                    var retVal = '<div class="warningNotVisible"></div>';
                    if(dataContext.invalid)
                    {
                        retVal = '<div class="warningVisible" title="' + dataContext.errormsg.join("\r\n") + '"></div>';
                    }
                    return retVal;
                }),
                "cancelRequestButton" : new ItemRenderer(function(row, cell, value, columnDef, dataContext)
                {
                    return '<div data="cancelRequestButton" class="el-btn-filled el-btn-grid cancelRequestButton">' + _this.options.cancelRequestLabel + '</div>';
                }),
                "pickListFormatter" : new ItemRenderer(function(row, cell, value, columnDef, dataContext)
                {
                    var label = columnDef.providerLabelMap[value];
                    return label;
                })
            });

            _this.selectedAccountRows = {};
            //Get items from dataProvider
            if(_this.options.dataProvider)
            {
                var dataProv = _this.options.dataProvider;
                
                for(var i=0; i < dataProv.length; i++)
                {
                    dataProv[i].id = dataProv[i].Id;
                    delete dataProv[i].Id;
                }
                
                //We user a different group by scheme for the approver grid
                _this.addPendingCoveragesToMap(dataProv);
                _this.reqGrid.addItems(dataProv);
                
            }

            _this.reqGrid.grid.init();

            _this.reqGrid.grid.invalidate()
            _this.reqGrid.dataProvider.refresh();

            console.log('Grid initialized');

            //default StateValue
            _this._context.local.state.put(_this.options.selectedRequestsCountProperty, 0);

            _this.footerDiv = $('<div class="coverageManagementFooter"></div>').appendTo(_this._context.target);

            _this.setListeners();

            _this.setFooter();//set footer on initial load

            if(_this.options.enableGrouping)
            {
                //See setGroupByDefault method to change default field
                _this.setGroupByDefault();
            }

            if(_this.options.stateFiltersEnabled)
            {                
                
                //Inline Coverage history filters. Had to do this since I needed _this
                var durationAndStatusFilter = function (item) 
                {  
                    var duration = item[_this.options.durationField];

                    var filterDuration = new Date();
                    var filterValDuration = _this._context.local.state.get(_this.options.durationFilterProperty);

                    filterDuration.setDate(filterDuration.getDate() - filterValDuration);

                    var status = item.Approval_Status__c ? item.Approval_Status__c.toLowerCase() : '';
                    var filterStatus = _this._context.local.state.get(_this.options.statusFilterProperty);;

                    if(duration < filterDuration)
                    {
                        return false;
                    }

                    if(status != filterStatus && filterStatus != 'all')
                    {
                        return false;
                    }

                    return true;
                }
                _this.reqGrid.setOption('additionalFilterFunction', durationAndStatusFilter);
            }

            // remove the saved items from the grid.  
            _this._context.local.eventDispatcher.on('coverageFinishedSaving',function(e, eventData)
            {
                for (var i = 0; i < eventData.covAIIds.length; i++)
                {
                    _this.processDelete(null, _this.reqGrid.dataProvider.getItemById(eventData.covAIIds[i]));
                }
                _this._context.local.state.put(_this.options.selectedRequestsCountProperty, 0);
            });

            _this.gridEditing = false;
            // disable cell edit for specific roles. 
            _this.reqGrid.grid.onBeforeEditCell.subscribe(function(e,args) 
            {
                if (args.column.field == "T1C_Base__Product__c" && _this._context.options.productMandatoryRoles.indexOf(args.item.T1C_Base__Role__c) < 0)
                {
                    _this.gridEditing = false;
                    return args.column.editable; 
                }

                _this.gridEditing = args.column.editable;
                    
                return args.column.editable;
            });

            //I had to create a latching system in order to allow for the dupe checks to properly happen
            //When a user is rapidly making changes: ST-2724
            _this.reqGrid.grid.onBeforeCellEditorDestroy.subscribe(function(e, args)
            {
                _this.gridEditing = false;

                //Set the timeout here, if the grid is rapidly editing, then we shouldnt be sending the event out
                //It's fine to just cancel the event since if the grid is editing, we have an editor open and that means that 
                //this listener will fire again after it is destroyed. We do need to store the changed rows in the case of no changes
                setTimeout(function(e, args)
                {
                    if(!_this.gridEditing)
                    {
                        //Grab all invalid rows as well
                        $.each(_this.reqGrid.dataProvider.getItems(), function(index, item)
                        {
                            if(item.invalid)
                            {
                                _this.changedRows.push(item);
                            }
                        });
                        
                        _this.triggerModifiedEvent(_this.changedRows);
                        _this.changedRows = [];
                    }
                },800);

            });

            if(_this.options.gridType == 'Request')
            {
                _this.addMainGridSelectedItems();
            }

            setTimeout(function(e, args)
            {
                _this.reqGrid.grid.resizeCanvas();
            });

        });
    };
    
    ManagerGridController.prototype.addPendingCoveragesToMap = function(dataProvider)
    {
        var _this = this;
        
        var accountField = _this.options.parentObjectField + '.' + _this.options.groupByField;   
        var groupByMergeString = '{' + accountField + '}';

        $.each(dataProvider, function(index, item)
        {
            var groupByVal = ACE.StringUtil.mergeParameters(groupByMergeString, item);

            if(!_this.coveragesMap[groupByVal])
            {
                _this.coveragesMap[groupByVal] = [];
            }
                
            _this.coveragesMap[groupByVal].push(item); 
        });

        //Commit coverage map to state
        _this._context.local.state.put(_this.options.saveToStateProperty, _this.coveragesMap);
    }

    ManagerGridController.prototype.setGroupByDefault = function()
    {
        var _this = this;
        var defaultGroupByField = _this.options.parentObjectField + '.' + _this.options.groupByField;
        //TODO Apply for a list of fields
        var defaultGetter = function(item)
        {
            var mergeStr = "{" + defaultGroupByField + "}";
            var mergeVal = ACE.StringUtil.mergeParameters(mergeStr, item);

            return mergeVal;
        }

        _this.reqGrid.dataProvider.setGrouping([
            {
                getter: defaultGetter,
                formatter: _this.groupByFormatterMap[_this.options.gridType],
                aggregators: [
                    //new Slick.Data.Aggregators.Avg('parent')
                ],
                aggregateCollapsed: false,
                lazyTotalsCalculation: true
            }
        ]);
    };

    ManagerGridController.prototype.setGroupByField = function(field)
    {
        var _this = this;

        //Empty the coverages map, we'll be filling it in using the groupby keys in the formatter
        //_this.coveragesMap[aggregate.value] = {};

        var defaultGroupByField = _this.options.parentObjectField + '.' + _this.options.groupByField;

        var defaultGetter = function(item)
        {
            var mergeStr = "{" + defaultGroupByField + "}";
            var mergeVal = ACE.StringUtil.mergeParameters(mergeStr, item);

            return mergeVal;
        }

        var subGetter = function(item)
        {
            var mergeStr = "{" + field + "}";
            var mergeVal = ACE.StringUtil.mergeParameters(mergeStr, item);

            return mergeVal;
        }
        //TODO Apply for a list of fields
        _this.reqGrid.dataProvider.setGrouping([
            {
                getter: defaultGetter,
                formatter: _this.groupByFormatterMap[_this.options.gridType],
                aggregators: [
                    //new Slick.Data.Aggregators.Avg('parent')
                ],
                aggregateCollapsed: false,
                lazyTotalsCalculation: true
            },
            {
                getter: subGetter,
                formatter: _this.groupByFormatterMap[_this.options.gridType],
                aggregators: [
                    //new Slick.Data.Aggregators.Avg('parent')
                ],
                aggregateCollapsed: false,
                lazyTotalsCalculation: true
            }
        ]);
    };

    ManagerGridController.prototype.setOption = function(name, val)
    {
        var _this = this;

        switch (name)
        {
            case "footerAccountsCount":
                _this.options.footerAccountsCount = val;
                _this.setFooter();
                break;
            case "footerCoveragesCount":
                _this.options.footerCoveragesCount = val;
                _this.setFooter();
                break;
            case "selectedMainGridItems":
                console.log("loading main grid selected Items");
                _this.addMainGridSelectedItems();
                break;
            case "stateDraftProperty" :
                console.log("loading draft");
                _this.loadDraftItems(val);
                break;
            default:
                break;
        }
    }

    ManagerGridController.prototype.loadDraftItems = function(draftMap)
    {
        //The draft is a map of CoverageFlatWrappers
        var _this = this;
        var newRows = [];

        $.each(draftMap, function(index, covWrapper)
        {
            newRows = newRows.concat(covWrapper.covAIs);
        })
        
        var rowsList = [];

        //Had to delete all of the items this way because of the referencing involved when deleting items
        $.each(_this.reqGrid.getItems(), function(index, row)
        {
            rowsList.push( (' ' + row.id).slice(1) );
        });

        $.each(rowsList, function(index, rowId)
        {
            _this.reqGrid.deleteItem(rowId);
        });
        
        _this.coveragesMap = draftMap;

        _this.reqGrid.addItems(newRows);
        
        _this._context.local.state.put(_this.options.saveToStateProperty, draftMap);

        _this._context.local.state.put("footerAccountsCount", Object.keys(_this.coveragesMap).length);
        _this._context.local.state.put("footerCoveragesCount", _this.reqGrid.dataProvider.getItems().length);
    }

    //Get the main grid selected items from state
    ManagerGridController.prototype.addMainGridSelectedItems = function()
    {
        var _this = this;
        var selItems = _this._context.local.state.get("selectedMainGridItems");
        if(!selItems || !Array.isArray( selItems ) || selItems.length < 0)
        {
            return;
        }

        var empId = _this._context.local.state.get(_this.options.employeeObj).Id
        var changedRows = [];

        //Get employee information 
        var rowData = _this.getEmpInfo();

        $.each(selItems, function(index, item)
        {
            var accountObj = item.data;

            if(Array.isArray(rowData))
            {
                for(var x=0; x < rowData.length; x++)
                {
                    changedRows.push( _this.addNewRow(accountObj.Id, empId, accountObj, rowData[x]) );
                }
            } 
            else
            {
                changedRows.push( _this.addNewRow(accountObj.Id, empId, accountObj, rowData) );
            }
            
        });

        //_this.reqGrid.grid.resizeCanvas();

        //Clear out all of the tokens after adding
        _this._context.local.state.put(_this.options.addAccountProperty, null);
        _this.triggerModifiedEvent(changedRows);
    }

    //Function to get the employee data and translate it into a data row. Used for anything that grabs values from the 
    //state pickers/employee object
    ManagerGridController.prototype.getEmpInfo = function(getFromState)
    {
        var _this = this;
        //Change to bulkOperationFieldMap?
        var empDefaults = getFromState ? _this._context.options.bulkOperationFieldMap : _this._context.options.employeeDefaults;
        var newObj = {};
        var newArr = [];
        var isArray = false;

        for (var prop in empDefaults) {
            var defaultVal = _this._context.local.state.get(empDefaults[prop]);
            
            isArray = isArray || Array.isArray(defaultVal);
            if(Array.isArray(defaultVal))
            {
                if(newArr.length < 1)
                {
                    var tempArr = [];
                    for(var i=0; i < defaultVal.length; i++)
                    {
                        var newItem = {};
                        newItem[prop] = defaultVal[i].data;
                        tempArr.push(newItem);
                    }
                    newArr = tempArr;
                }
                else
                {
                    var mergedArray = [];
                    for(var x=0; x < newArr.length; x++)
                    {
                        var obj = newArr[x];
                        for(var y=0; y < defaultVal.length; y++)
                        {
                            var val = defaultVal[y].data;
                            if(prop == 'T1C_Base__Product__c')
                            {
                                var role = _this._context.local.state.get(empDefaults['T1C_Base__Role__c']);
                                val = _this.checkProduct(role, val);
                                if(!val)
                                {
                                    continue;
                                }
                            }

                            var tempObj = {};
                            tempObj[prop] = val;

                            tempObj = $.extend(tempObj, obj);
                            mergedArray.push(tempObj);
                        }
                    }

                    if(mergedArray.length > 0)
                    {
                        newArr = mergedArray;
                    }
                }
            }
            else
            {
                if(prop == 'T1C_Base__Product__c')
                {
                    var role = _this._context.local.state.get(empDefaults['T1C_Base__Role__c']);
                    defaultVal = _this.checkProduct(role, defaultVal);
                }
                newObj[prop] = defaultVal;
            }
        }

        if(isArray)
        {
            for(var x=0; x < newArr.length; x++)
            {
                var obj = newArr[x];
                obj = $.extend(obj, newObj);
            }

            return newArr;
        }
        return newObj;
    };

    //Method to check the role in state to see if it matches 
    ManagerGridController.prototype.checkProduct = function(role, product)
    {
        var _this = this;
        if (_this._context.options.productMandatoryRoles.indexOf(role) < 0)
        {
            return null;
        }

        //Product specific behaviour; if there is no default emp product, just default to the first product
        var defProduct = _this._context.local.state.get('employeeProducts');
        defProduct = defProduct.length > 1 ? defProduct[1].id : null;
        product = product && product.length > 0 ? product : defProduct;

        return product
    }

    ManagerGridController.prototype.setListeners = function()
    {
        var _this = this;
        //Add account Listener
        if(_this.options.gridType == 'Request')
        {
            _this._context.local.eventDispatcher.on(_this.options.addAccountEvent, function(e, eventData)
            {
                //TODO Maybe change to the state pickers?
                //Add the default values from the employee object into the grid when adding a new row
                //from sources outside the bulk action
                var empDefaults = _this._context.options.employeeDefaults;
                var rowData = _this.getEmpInfo();

                var accountObjs = _this._context.local.state.get(_this.options.addAccountProperty);
                var empId = _this._context.local.state.get(_this.options.employeeObj).Id;
                var changedRows = [];

                $.each(accountObjs, function(accountID, accountObj)
                {
                    if(Array.isArray(rowData))
                    {
                        for(var x=0; x < rowData.length; x++)
                        {
                            changedRows.push( _this.addNewRow(accountObj.Id, empId, accountObj, rowData[x]) );
                        }
                    } 
                    else
                    {
                        changedRows.push( _this.addNewRow(accountID, empId, accountObj, rowData) );
                    }
                });

                //_this.reqGrid.grid.resizeCanvas();

                //Clear out all of the tokens after adding
                _this._context.local.state.put(_this.options.addAccountProperty, null);
                _this.triggerModifiedEvent(changedRows);
            });
        
            _this.changedRows = [];
            _this.reqGrid.grid.onCellChange.subscribe(function(e, args)
            {            
                // clear out product if the role requires null product.
                if (_this._context.options.productMandatoryRoles.indexOf(args.item.T1C_Base__Role__c) < 0)
                {
                    args.item.T1C_Base__Product__c = "";
                    _this.reqGrid.dataProvider.refresh();
                }
                else 
                {
                    // add default product if the role requires product and product doesn't exist. 
                    if (args.item.T1C_Base__Product__c == null || args.item.T1C_Base__Product__c == "")
                    {
                        var empDefaults = _this._context.options.employeeDefaults;
                        var defaultProduct = _this._context.local.state.get(empDefaults.T1C_Base__Product__c);

                        //Product specific behaviour; if there is no default emp product, just default to the first product from the list
                        //The first item will always be the null item so we go the the next item
                        var defProduct = _this._context.local.state.get('employeeProducts');
                        defProduct = defProduct.length > 1 ? defProduct[1].id : null;
                        defaultProduct = defaultProduct && defaultProduct.length > 0 ? defaultProduct : defProduct;

                        if (defaultProduct != null && defaultProduct != "")
                        {
                            args.item.T1C_Base__Product__c = defaultProduct;
                            _this.reqGrid.dataProvider.refresh();
                        }
                    }
                }

                _this.changedRows.push( args.item );
            });

            //This listener is responsible for toggling the checkboxes of the normal and groupby rows
            _this._context.local.eventDispatcher.on('toggleGridMode', function(e, eventData)
            {
                _this.clearSelectedItems();

                _this.gridMode = eventData.mode;
                _this._context.local.state.put('bulkCoverageMode',_this.gridMode);

                _this.reqGrid.dataProvider.refresh();
                _this.reqGrid.grid.invalidate();
            });

        
            _this._context.local.eventDispatcher.on(_this.options.bulkUpdateEvent, function(e, eventData)
            {
                _this.bulkUpdateGrid();
            });
        }
        
        _this.reqGrid.grid.onClick.subscribe(function(e, args)
        {
            //Used to detect groupby row checkbox clicks
            var clickedItem = args.grid.getDataItem(args.row);

            switch( $(e.target).attr('data') )
            {
                case 'groupByCheckBox':
                    if(clickedItem.groupingKey)
                    {
                        if(_this.options.gridType == "Approver")
                        {
                            //Select all children
                            _this.reqGrid.dataProvider.beginUpdate();
                            $.each(clickedItem.rows, function(index, item)
                            {
                                item._customIsSelected = e.target.checked;
                                _this.reqGrid.dataProvider.updateItem(item.id, item)
                            });
                            _this.reqGrid.dataProvider.endUpdate();
                            var selectedRequestsCount = _this.reqGrid.getSelectedItems().length;
                            _this._context.local.state.put(_this.options.selectedRequestsCountProperty, selectedRequestsCount);
                        }
                        else
                        {
                            var item = _this.coveragesMap[clickedItem.groupingKey];
                            item.selected = e.target.checked;
                        }
                    }
                    break;
                //The delete button should only be for the Requester grid
                case 'deleteButton':
                    //We only really want to send the acais grouped in this parent since we need to check for dupes
                    //and dupes do not occur outside of the account that the deleted row is grouped under
                    var groupByVal = "{" + _this.options.parentObjectField + "." + _this.options.groupByField + "}";
                    groupByVal = ACE.StringUtil.mergeParameters(groupByVal, clickedItem);

                    _this.processDelete(args, clickedItem);

                    var cov = _this.coveragesMap[groupByVal] ? _this.coveragesMap[groupByVal].covAIs : [];
                    
                    _this.triggerModifiedEvent(cov);
                    break;
                case 'cancelRequestButton':
                    _this.cancelRequest(args, clickedItem);
                    break;
                case 'addRow':
                    var cov = _this.coveragesMap[clickedItem.groupingKey];
                    var acntId = cov.covRecord.T1C_Base__Account__c;
                    var empId = _this._context.local.state.get(_this.options.employeeObj).Id
                    var acntObj = cov.covRecord.T1C_Base__Account__r;
                    var newObj = _this.getEmpInfo(false);

                    var changedRows = [];
                    changedRows.push( _this.addNewRow(acntId, empId, acntObj, newObj) );

                    //This is thrown primarily for dupe checking
                    _this.triggerModifiedEvent(changedRows);
                    break;
                default:
                    console.log('Grid Click not groupBy');
            }
        });

        _this.reqGrid.onRowSelectionChange(function(e, args)
        {
            var selectedRequestsCount = _this.reqGrid.getSelectedItems().length;
            _this._context.local.state.put(_this.options.selectedRequestsCountProperty, selectedRequestsCount);
        });

        $(window).resize(function()
        {
            _this.reqGrid.grid.resizeCanvas();
        });

        _this._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e, eventData)
        {
            if (eventData != null)
            {
                if(eventData.property == _this.options.groupByFilterProperty)
                {
                    var groupBy = _this._context.local.state.get(_this.options.groupByFilterProperty);
                    var defaultGroupByField = _this.options.parentObjectField + '.' + _this.options.groupByField;
    
                    //If filter property is default grouping then only group by parent                
                    groupBy != defaultGroupByField ? _this.setGroupByField( groupBy ) : _this.setGroupByDefault();
                }
                else if( _this.options.stateFiltersEnabled &&
                    (eventData.property == _this.options.durationFilterProperty) || (eventData.property == _this.options.statusFilterProperty))
                {
                    _this.reqGrid.dataProvider.refresh();
                }
            }            
        });

        if(_this.options.validationEnabled)
        {
            _this._context.local.eventDispatcher.on(ACE.AML.Event.STATE_VALIDATION_CHANGE, function(e, eventData)
            {
                //console.log('Quack');
                if(eventData.results && eventData.results.hasOwnProperty( 'validatedRows' ))
                {
                    var numValidRows = _this._context.local.state.get('numValidRows');
                    _this.reqGrid.dataProvider.beginUpdate();
                    $.each(eventData.results.validatedRows, function(index, row)
                    {
                        //Find row by Id and render the warning on it
                        var rowData = _this.reqGrid.dataProvider.getItemById(row.value.id);

                        if(!rowData)
                        {
                            return true;
                        }

                        //if the validity of the row has changed, modify the num valid rows
                        //I negated twice to cover undefined
                        if(rowData.invalid != !row.valid)
                        {
                            if(row.valid)
                            {
                                numValidRows = numValidRows + 1;
                            } else if(!row.valid)
                            {
                                numValidRows = numValidRows - 1;
                            }
                        }

                        rowData.invalid = !row.valid;
                        rowData.errormsg = row.messages;
                        
                        //dedupe the conflicting emps
                        var conflEmps = rowData.conflictingEmps ? rowData.conflictingEmps.split(',') : [];

                        if(row.conflictName && !conflEmps.includes(row.conflictName))
                        {
                            rowData.conflictingEmps = rowData.conflictingEmps ? rowData.conflictingEmps + ', ' + row.conflictName : row.conflictName; 
                        }

                        if(row.valid)
                        {
                            delete rowData.invalid;
                            delete rowData.errormsg;
                            delete rowData.conflictingEmps;
                        }

                        _this.reqGrid.dataProvider.updateItem(row.value.id, rowData);
                    });
                    _this.reqGrid.dataProvider.endUpdate();

                    numValidRows = Math.max(0, numValidRows);
                    _this._context.local.state.put('numValidRows', numValidRows);
                }

            });
        }

        var selectedAccounts = _this._context.local.state.get(_this.options.addAccountProperty);
        if(_this.options.gridType == 'Request' && selectedAccounts && Object.keys(selectedAccounts).length > 0)
        {
            //Add the main grid selected items and the pre-loaded accounts
            _this._context.local.eventDispatcher.trigger(_this.options.addAccountEvent);
        }
    };

    ManagerGridController.prototype.showHandler = function(args, item)
    {
        var _this = this;
        if(_this.reqGrid)
        {
            //There's an issue where invisible text boxes do not respond after being set as the text filter
            _this.reqGrid.setOption('textFilter', $(_this.options.textFilter).find('input'));
            _this.reqGrid.grid.resizeCanvas();
        }
    };

    ManagerGridController.prototype.resizeHandler = function(args, item)
    {
        var _this = this;
        if(_this.reqGrid)
        {
            _this.reqGrid.grid.resizeCanvas();
        }
    };

    ManagerGridController.prototype.processDelete = function(args, item)
    {
        var _this = this;

        //The grouping only supports one layer for now
        //TODO figure out a way to delete rows for sub groups
        if(item.groupingKey)
        {
            $.each(item.rows, function(index, row)
            {   
                _this.reqGrid.deleteItem(row.id);
            });
            delete _this.coveragesMap[item.groupingKey];
        }
        else
        {
            _this.reqGrid.deleteItem(item.id);
            //Now delete it from the coverages map
            //Find the external Id of its parent record
            var groupByField = '{' + _this.options.parentObjectField + '.' + _this.options.groupByField + '}';
            var externalId = ACE.StringUtil.mergeParameters(groupByField, item);
            var wrapper = _this.coveragesMap[externalId];

            if(wrapper)
            {
                //When the grid type is Approve, the coverages map only has lists of covAIs
                //When type is Request, they are Coverage Flat Wrappers (see the Interfaces class)
                var delIndex = null;
                var covsList = _this.options.gridType == 'Request' ? wrapper.covAIs : wrapper;
                $.each(covsList, function(index, row)
                {
                    if(row.id == item.id)
                    {
                        delIndex = index;
                        return false;
                    }
                })

                covsList.splice(delIndex, 1);

                if(covsList.length < 1)
                {
                    delete _this.coveragesMap[externalId];
                }
            }

        }

        _this._context.local.state.put(_this.options.saveToStateProperty, _this.coveragesMap);
        _this._context.local.state.put("footerAccountsCount", Object.keys(_this.coveragesMap).length);
        _this._context.local.state.put("footerCoveragesCount", _this.reqGrid.dataProvider.getItems().length);
    };
    
    ManagerGridController.prototype.cancelRequest = function(args, item)
    {
        var _this = this;
        var yesString = ACE.Locale.getBundleValue('YES');
        var noString = ACE.Locale.getBundleValue('NO');
        ACEConfirm.show
        (
            _this.options.cancelRequestMessage,//TODO Localize
            ACE.Locale.getBundleValue('WARNING'),
            [ yesString + "::save-button confirmButton",  
                noString + "::el-btn-lg el-btn-outline"],
            function(result)
            {
                if(result == noString)
                {
                    return;
                }

                console.log('Cancel' + item.id);
                //TODO 
                // Remove from grid and state
                _this.reqGrid.deleteItem(item.id);

                var empId = _this._context.local.state.get(_this.options.employeeObj).Id
                var acntId = item.T1C_Base__Account_Coverage__r.T1C_Base__Account__c;

                var coverageKey = acntId + ':' + empId;
                var covs = _this.coveragesMap[coverageKey];
                var delACAI = null;
                for (var index in covs)
                {
                    var acai = covs[index];
                    if(acai.id == item.id)
                    {
                        delACAI = acai;
                        break;
                    }
                }

                delete delACAI;
                var covsList = [];

                covsList.push(item.id);
                // Remove from backend
                ACE.Remoting.call( "CoverageApprovalManagerController.cancelPendingRequests", [covsList], function (result, event)
                {
                    if (event.status)
                    {			
                        console.log('Request Canceled');
                    }
                    else
                    {
                    }									
                });
                
                _this._context.local.state.put(_this.options.footerAccountPropertyName , _this.reqGrid.dataProvider.getGroups().length);
                _this._context.local.state.put(_this.options.footerCoveragePropertyName , _this.reqGrid.dataProvider.getItems().length);
            }
        );
    };

    //Row Data is used for the bulk adds
    ManagerGridController.prototype.addNewRow = function(accountID, empId, accountObj, rowData)
    {
        var _this = this;
        if(accountID && accountID.length > 0 && empId && empId.length > 0)
        {
            //TODO: Dupe checking here

            //////////////////////////////////
            //Store this in a coverage flat wrapper using the ACs projected External ID
            var acExternalId = accountID + ':' + empId ;
            var covFlatWrapper = _this.coveragesMap[acExternalId];

            if(!covFlatWrapper)
            {
                var cloneAcntObj = $.extend({},accountObj);
                var acRecord =
                {
                    //sObjectType : _this.options.coverageType,
                    T1C_Base__Account__c : accountID,
                    T1C_Base__Account__r : cloneAcntObj,
                    T1C_Base__Employee__c : empId
                };

                acRecord[_this.options.acExternalIdField] = acExternalId;
                covFlatWrapper =
                {
                    covRecord : acRecord,
                    covAIs    : [],//Dont add this new row to the list yet, we'll do that before saving using externalIDs again
                    //revoke;
                    covType   : _this.options.coverageType,//Account Coverage or Contact coverage (use API name)
                    infoType  : _this.options.additionalInfoType//ACAI or CCAI (use API name)
                }
            }

            var newRow =
            {
                id : "newRow" + _this.newRowIndex++,
                sObjectType : _this.options.additionalInfoType
            };

            newRow[_this.options.parentObjectField] = covFlatWrapper.covRecord;

            newRow = $.extend(newRow, rowData);

            covFlatWrapper.covAIs.push(newRow);

            //Add to our internal map, then the grid
            _this.coveragesMap[acExternalId] = covFlatWrapper;

            _this.reqGrid.addItems(newRow);

            //Commit coverage map to state
            _this._context.local.state.put(_this.options.saveToStateProperty, _this.coveragesMap);

            _this._context.local.state.put("footerAccountsCount", Object.keys(_this.coveragesMap).length);
            _this._context.local.state.put("footerCoveragesCount", _this.reqGrid.dataProvider.getItems().length);

            return newRow;
        }
        return null;
    };

    ManagerGridController.prototype.bulkUpdateGrid = function()
    {
        //The assumption for bulk updates is that we already have the ac records
        //no 'new' accounts can be added using this feature, only new acai records
        var _this = this;
        //Get the values from the picklists from state using the configs
        var rowData = _this.getEmpInfo(true);

        //Now get the selections. Note that this changes between Add and Update modes
        //acExternalId = accountID + empId
        //_this.coveragesMap[acExternalId] = covFlatWrapper;
        var changedRows = [];
        //getVisibleRange()
        if(_this.gridMode.toLowerCase() == 'add')
        {
            $.each(_this.coveragesMap, function(index, item)
            {
                if(item.selected)
                {
                    var acntId = item.covRecord.T1C_Base__Account__c;
                    var empId = _this._context.local.state.get(_this.options.employeeObj).Id
                    var acntObj = item.covRecord.T1C_Base__Account__r;
                    
                    if(Array.isArray(rowData))
                    {
                        for(var x=0; x < rowData.length; x++)
                        {
                            changedRows.push( _this.addNewRow(acntObj.Id, empId, acntObj, rowData[x]) );
                        }
                    } 
                    else
                    {
                        changedRows.push( _this.addNewRow(acntId, empId, acntObj, rowData) );
                    }
                }
            });
        }else
        {
            $.each(_this.reqGrid.getSelectedItems(), function(index, item)
            {
                item = $.extend(item, rowData);
                _this.reqGrid.updateItem(item.id, item);

                changedRows.push( item );
            });
        }

        _this.triggerModifiedEvent(changedRows);
    };

    //Whenever rows are modified, we should also send invalid rows to be validated again
    ManagerGridController.prototype.triggerModifiedEvent = function(changedRows)
    {
        var _this = this;
        var rowData = [];
        rowData = rowData.concat(changedRows);
        //get all invalid rows that are not already in the array.
        var changedIds = [];
        $.each(changedRows, function(index, item)
        {
            changedIds.push(item.id);
        });

        $.each(_this.reqGrid.dataProvider.getItems(), function(index, row)
        {
            if(row.invalid && !changedIds.includes(row.id))
            {
                rowData.push(row);
            }
        });

        _this._context.local.state.put(_this.options.saveToStateProperty, _this.coveragesMap);
        _this._context.local.eventDispatcher.trigger('coverageRowsModified', {"changedRows" : rowData});
    }


    ManagerGridController.prototype.requestModeCheckBoxUpdater = function()
    {
        var _this = this;
        return function(isSelected, dispatchEvent, updateHeaderCheckbox)
        {
            var length = this.dataProvider.getLength();
            var countLength = 0;

            if(_this.gridMode.toLowerCase() == 'add')
            {
                if (length !== 0)
                {
                    for (var i = 0 ; i < length ; i++)
                    {
                        var itemRow = _this.reqGrid.grid.getDataItem(i);

                        if (itemRow.groupingKey)
                        {
                            var covWrapper = _this.coveragesMap[itemRow.groupingKey];
                            covWrapper.selected = isSelected;
                        }
                    }
                }

                _this.reqGrid.dataProvider.refresh();
            }
            _this.reqGrid.superUpdateCheckBoxSelection(isSelected, dispatchEvent, updateHeaderCheckbox);
        }
    }

    //Had to do it this way because the context is lost once this method is called
    ManagerGridController.prototype.approveModeCheckBoxUpdater = function()
    {
        var _this = this;
        return function(isSelected, dispatchEvent, updateHeaderCheckbox)
        {
            var length = this.dataProvider.getLength();

            if (length !== 0)
            {
                _this.reqGrid.dataProvider.beginUpdate()
                for (var i = 0 ; i < length ; i++)
                {
                    var itemRow = _this.reqGrid.grid.getDataItem(i);

                    //If the header checkbox is clicked, we should select all
                    if (itemRow.groupingKey)
                    {
                        itemRow.selected = isSelected;
                    }
                }
                _this.reqGrid.dataProvider.endUpdate()
            }

            _this.reqGrid.dataProvider.refresh();
            
            _this.reqGrid.superUpdateCheckBoxSelection(isSelected, dispatchEvent, updateHeaderCheckbox);
        }
    }

    ManagerGridController.prototype.clearSelectedItems = function(colsArray)
    {
        this.reqGrid.clearSelectedItems();

        $.each(this.coveragesMap, function(index, item)
        {
            item.selected = false;
        });

    }

    ManagerGridController.prototype.addFormattersAndEditors = function(colsArray)
    {
        var _this = this;
        //For picklists, we must keep a reference to their values so that we can filter for them later
        _this.pickListFilterMap = {};
        _this.statePicklistsFields = [];
        //For each FieldType, set the editor and formatter
        $.each(colsArray, function(index, column)
        {
            if(!column.fieldType)
            {
                //skip to next item if there's no field type
                return true;
            }

            switch( column.fieldType.toLowerCase() )
            {
                case "lookup":
                    column.editor = Slick.Editors.AutoComplete;
                    break;
                case "date" :
                    column.editor = Slick.Editors.Date;
                    break;
                case "picklist" :
                    column.editorDataProvider = column.editorOptions.dataProvider;
                    column.editorOptions =
                    {
                        dataField    : 'data',
                        getValOnly   : true
                    };
                    column.editor = Slick.Editors.ACEMenuButton;
                    column.formatter = "pickListFormatter";
                    //Since the editor only returns the raw value (always in english)
                    //we need to map it out to its labels so that we can present it in the correct language.
                    //See the pickListFormatter in the setColumns() call for more context
                    column.providerLabelMap = {};
                    $.each(column.editorDataProvider, function(index, item)
                    {
                        column.providerLabelMap[item.data] = item.label;
                    });

                    break;
                case "statepicklist" :
                    //state picklist grabs a dataprovider from state
                    column.editorOptions =
                    {
                        defaultValue : column.defaultValue,
                        dataField    : column.listDataField,
                        getValOnly   : true
                    };
                    column.editorDataProvider = [];
                    
                    column.editorDataProvider = 
                        column.editorDataProvider.concat( _this._context.local.state.get(column.stateProvider) );
                    //For some reason the editor doesnt let us set a label field
                    //Convert all designated name fields to 'label'
                    $.each(column.editorDataProvider, function(index, item)
                    {
                        item.label = item[column.labelField];
                        _this.pickListFilterMap[item.data] = item.label;
                    });
                    _this.statePicklistsFields.push(column.field);
                    column.editor = Slick.Editors.ACEMenuButton;
                    column.formatter = "statePicklist";
                    break;
                case "deletebutton":
                    column.formatter = 'deleteButton';
                    column.maxWidth = 30;
                    break;
                case "validationindicator":
                    column.formatter = 'errorRenderer';
                    column.maxWidth = 30;
                    break;
                default:
                    column.editor = Slick.Editors.Text
            }

            //Next add the delete editor

        });
    };

    ManagerGridController.prototype.setFooter = function()
    {
        var _this = this;
        var countObj =
        {
            'footerAccountsCount' : _this.options.footerAccountsCount || 0,
            'footerCoveragesCount' : _this.options.footerCoveragesCount || 0
        }

        var mergeText = _this.options.footerText;
        var footerText = ACE.StringUtil.mergeParameters(mergeText, countObj);

        _this.footerDiv.text(footerText);
    };

    ACE.AML.ControllerManager.register("extensions:managerGridController", ManagerGridController);
	//# sourceURL=ManagerGridController.js
})(jQuery);