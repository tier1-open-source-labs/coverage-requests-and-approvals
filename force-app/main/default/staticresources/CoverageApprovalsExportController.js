/**
 * Name: CoverageApprovalsExportController.js 
 * Description: Controller that calls apex controller to upsert records for the
 *              coverage approvals form
 *
 * Confidential & Proprietary, 2020 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */

(function($)
{ 
    var BASE_CONFIG = 
    {
        reportName : "Coverage Requests",
        columns : [],
        workBookTitle : "Coverages",
        filterMap   : {},
        dataProviderProperty : "coveragesMap"
    }

    function CoverageApprovalsExportController(context)
    {
        this._context = context;	
        var _this = this;
        _this.options = $.extend({}, BASE_CONFIG, context.options);
         //We do this so that we have the formatters        
        _this.formattedCols = _this.addFormatters(_this.options.columns);
        this._context.readyHandler();
        
    }
    
    CoverageApprovalsExportController.prototype.execute = function(e, eventData, callback)
    {
        var _this = this;
        var reportDataProvider = [];
        //Assemble the header row first
        var headerRow = [];
        var headerFieldMap = {};
        $.each(_this.formattedCols,function(index, column)
        {
            if(column.name.length > 0)
            {
                headerRow.push(column.name);
                headerFieldMap[column.name] = column.field;
            }
        });

        reportDataProvider.push(headerRow);

        var filter = _this.options.filterMap[eventData.filter];
        var rows = 0;

        var coverageMap = _this._context.local.state.get(_this.options.dataProviderProperty);

        var dataProvider = this.extractACAIs(coverageMap);

        $.each( dataProvider, function(row, item)
        {
            //filter out the selected or the conflicts here
            if( filter && (item[filter] == null || !item[filter]) )
            {
                return true;
            }

            var valueRow = [];
            $.each(_this.formattedCols,function(index, column)
            {
                var stringVal = "";
                if(!headerFieldMap[column.name])
                {
                    return true;
                }

                var param = '{' + column.field + '}';
                var val = ACE.StringUtil.mergeParameters(param, item);

                if(column.formatter)
                {
                    stringVal = column.formatter(row, null, val, column, item);
                }else
                {
                    stringVal = val;
                }
                valueRow.push(stringVal);
            });

            reportDataProvider.push(valueRow);
            rows++;
        });

        if(rows < 1)
        {
            //toast message?
            callback(true);
            return;
        }

        var filename = this.options.reportName;

        //Create the target Companies worksheet
        var workbook = ACE.Excel.Builder.createWorkbook();
		var worksheetTitle = _this.options.workBookTitle;
		var worksheet = workbook.createWorksheet({ name: worksheetTitle });

        worksheet.setData(reportDataProvider);
        workbook.addWorksheet(worksheet);
        
        ACE.Excel.Builder.createAndSaveFile(workbook, null, filename + ".xlsx");

        callback(true);
    }
  
    CoverageApprovalsExportController.prototype.addFormatters = function(colsArray)
    {
        var _this = this;

        var cols = [];
        var statePickListFormatter = function(row, cell, value, columnDef, dataContext)
        {
            //Need to go back to the field in state and match the data Field for a name
            var stateList = _this._context.local.state.get(columnDef.stateProvider);

            var nameField  = columnDef.labelField;
            var matchField = columnDef.listDataField;

            var nameVal = "";
            if(stateList)
            {
                $.each(stateList, function(index, item)
                {
                    if(item[matchField] == value && value.length > 0)
                    {
                        nameVal = item[nameField];
                        return false;
                    }
                });
            }

            return nameVal;
        };

        //Needed because ACAI records dont actually have a reference to their parent Account. Only the AC record does
        var parentAccountFormatter = function(row, cell, value, columnDef, dataContext)
        {
            var nameVal = dataContext.T1C_Base__Account_Coverage__r.T1C_Base__Account__r.Name

            return nameVal;
        };

        //For each FieldType, set the formatter
        $.each(colsArray, function(index, column)
        {
            if(!column.fieldType)
            {
                //skip to next item if there's no field type
                return true;
            }

            switch( column.fieldType.toLowerCase() )
            {
                case "lookup":
                    column.editor = Slick.Editors.AutoComplete;
                    break;
                case "statepicklist" :
                    column.formatter = statePickListFormatter;
                    break;
                case "parent":
                    column.formatter = parentAccountFormatter;
                    break;
                default:
            }
            cols.push(column);
        });

        return cols;
    };

    CoverageApprovalsExportController.prototype.extractACAIs = function(covs)
    {
        var dataProvider = [];
        $.each(covs, function(externalId, item)
        {
            dataProvider = dataProvider.concat(item.covAIs);
        });

        return dataProvider;

    }
    

    ACE.AML.ControllerManager.register("extensions:CoverageApprovalsExportController", CoverageApprovalsExportController);
//# sourceURL=CoverageApprovalsExportController.js
})(jQuery);