//=============================================================================
/**
 * Name: CoverageApprovalsLocale.js
 * Description: Custom locale class to override the en_US locale bundle strings for client customization. Used to expose the locale keys
 * 				available for the client to override
 *
 * Confidential & Proprietary, 2017 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//============================================================================
(function()
{
    //ACE.Locale.loadBundle(ACE.Salesforce.userLocale);

	ACE.Locale.registerBundle
	(
		"en_US",
		{
		// Please do not translate anything that is enclosed in braces {} or in html tags <>//
        ACCOUNT                  : "Account",
        ACCOUNT_SEARCH_HOVER     : "Type an Account name to search",
        ACTION                   : "Action",
        ADD                      : "Add",
        ADD_CLIENT               : "Add New Client",
        ALL                      : "All",
        APPROVED                 : "Approved",
        APPROVE_BUTTON           : "<span>Approve ({selectedRequestsCount})</span>",
        APPROVE_MESSAGE          : "You have selected {selectedRequestsCount} requests to approve. Do you wish to proceed?",
        CANCEL                   : "Cancel",
        CANCEL_REQUEST           : "Cancel Request",
        CANCEL_REQUEST_WARNING   : "Cancel this request?",
        CONTINUE                 : "Continue",
        DISCARD                  : "Discard",
        DRAFT_MESSAGE            : "You have a pending draft, would you like to continue or discard the draft?",
        DURATION                 : "Duration",
        EMPLOYEE                 : "Employee",
        ENTER_NOTES              : "Enter notes",
        EXPORT_CONFLICTS         : "Export Conflicts",
        EXPORT_SELECTED          : "Export Selected",
        FILTER                   : "Filter",
        GLOBAL_VALIDAITON        : "Global Region is only available for Relationship Manager Role.",
        GRID_FOOTER              : "{footerAccountsCount} Accounts, {footerCoveragesCount} Coverage Requests",
        GROUP_BY                 : "Group By",
        INCLUDE_CLIENT_HIERARCHY : "Include entire client hierarchy",
        NO                       : "No",
        PAST_12_MONTHS           : "Past 12 months",
        PAST_14_DAYS             : "Past 14 days",
        PAST_30_DAYS             : "Past 30 days",
        PAST_60_DAYS : "Past 60 days",
        PAST_6_MONTHS : "Past 6 months",
        PAST_7_DAYS : "Past 7 days",
        PAST_90_DAYS : "Past 90 days",
        PAST_9_MONTHS : "Past 9 months",
        COVERAGE_PENDING_REQUEST_TAB : "Pending Requests",
        PRODUCT: "Product",
        PRODUCT_NOT_ALLOWED : "You cannot choose to cover a product with the selected Role",
        PRODUCT_VALIDATION : "You must choose a product to cover with the selected Role",
        REASON_OPTIONAL : "Reason (optional)",
        REGION: "Region",
        REGION_VALIDATION : "A region must be defined",
        REJECTED: "Rejected",
        REJECT_BUTTON: "<span>Reject ({selectedRequestsCount})</span>",
        REJECT_MESSAGE : "You have selected {selectedRequestsCount} requests to reject. Do you wish to proceed?",
        RENOTIFY_APPROVER : "Re-notify Approver",
        RENOTIFY_SUCCESS : "Email notification sent",
        REQUESTING_AS_FORMAT : "Requesting as {Name}. [[This requires approval from {T1_Coverage_Approver__r.Name}]]",
        ROLE : "Role",
        ROLE: "Role",
        ROLE_VALIDATION : "A role must be defined",
        SAVE_DRAFT : "Save as Draft",
        STATUS : "Status",
        SUBMIT_APPROVAL : "Submit for Approval",
        TEXT_FILTER_PLACEHOLDER : "Enter text to filter results",
        UPDATE : "Update",
        YES : "Yes"
	});
    
    ACE.Locale.loadBundle(ACE.Salesforce.userLocale);
})();

//# sourceURL=CoverageApprovalsLocale.js