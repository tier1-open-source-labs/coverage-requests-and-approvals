/**
 * Name: CoverageApprovalsRowValidationController.js 
 * Description: Controller that validates a single row.
 *              There are limitations in place so we can't do all rows at once,
 *              they are to evaluated on update/create only
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */

(function($)
{  

    function CoverageApprovalsRowValidationController(context)
    {
        this._context = context;	
        var __this = this;
        __this.options = $.extend({}, context.options);
        this._context.readyHandler();
    }
    
    CoverageApprovalsRowValidationController.prototype.execute = function(e, eventData, callback)
    {
        var __this = this;
        
        var executeIf = __this.options.executeIf;

        //This function takes in a an array with the expected item structure:
        /*
        {
            "property": "T1C_Base__Region__c",
            "values": [ null ],
            "operators": [ "!=" ],
            "message": "You must have at least one coverage request",
            "requiredIf" : [
            {
                "property": "T1C_Base__Region__c",
                "values": [ null ],
                "operators": [ "!=" ]
            }
            ]
        }
        */
        var processRuleSet = function(ruleSet, value)
        {
            var msgList = [];
            var proceed = true;
            $.each(ruleSet, function(index, rule)
            {
                var executeRule = true;
                //Evaluate the requiredIf first
                if(rule.requiredIf)
                {
                    var reqOp = rule.requiredIf;
                    executeRule = processRuleSet(reqOp, value).valid;
                }

                if(executeRule)
                {
                    //Value won't necessarily be local state so we can't assume that we have state.get
                    var stateVal = value;
                    $.each(rule.property.split('.'), function(index, subProp)
                    {
                        stateVal = stateVal[subProp];
                        if(!stateVal)
                        {
                            stateVal = '';
                            return false;
                        }
                    });

                    $.each(rule.operators, function(index, op)
                    {   
                        var compVal = rule.values[index];
                        if(typeof compVal == "string")//Locale keys can also be passed in these value properties
                        {
                            compVal = ACE.Locale.parse(compVal);
                        }

                        // substitute {state.PROP} with property from state. 
                        if (compVal != null && typeof compVal == "string" && compVal.match("^\{state.") && compVal.match("\}$")) 
                        {
                            compVal = compVal.replace(/^\{state\./g, "").replace(/\}$/g, "");
                            compVal = __this._context.local.state.get(compVal);
                        }

                        var rulePassed = COMPARITOR_MAP[op](stateVal, compVal);
                        proceed = proceed && rulePassed;
                        //If the rule didn't pass, add its message to the message array
                        if(!rulePassed && rule.message)
                        {
                            var errorMsg = ACE.Locale.parse(rule.message);
                            msgList.push(errorMsg)
                        }
                    });
                }
            });

            var result = {
                "valid" : proceed,
                "messages" : msgList,
                "value"    : value
            };

            return result
        };

        try
        {
            var validatedRows = [];
            $.each(eventData.changedRows, function(index, rowVal)
            {
                if( typeof executeIf != 'undefined' && Array.isArray(executeIf))
                {
                    var proceed = processRuleSet(executeIf,rowVal).valid;
                    
                    if(!proceed)
                    {
                        return true;
                    }
                }

                //Row logic here

                var result = processRuleSet(__this.options.rules, rowVal);
                
                validatedRows.push(result);
            });
            //If we have invalid rows, throw the event
            var returnData = 
            {
                "results" : 
                {
                    "validatedRows" : validatedRows
                }
            };
            __this._context.local.eventDispatcher.trigger(ACE.AML.Event.STATE_VALIDATION_CHANGE, returnData);

            //We store our results in the eventData to make sure that followup event handlers can see what we validated
            eventData.validatedRows = validatedRows;
            callback(true)
        }
        catch(e)
        {
            console.log('Error in Execute If: ');
            console.log(e);
            callback(false);
            return false;
        }
    }
    
    var equalsComp = function(val1, val2)
    {
        var comp1 = typeof val1 == 'undefined' ? null : val1;
        var comp2 = typeof val2 == 'undefined' ? null : val2;
        
        return comp1 == comp2;
    };
    
    var notEqualsComp = function(val1, val2)
    {
        return !equalsComp(val1, val2);
    };
    
    var greaterComp = function(val1, val2)
    {
        var comp1 = typeof val1 == 'undefined' ? null : val1;
        var comp2 = typeof val2 == 'undefined' ? null : val2;
        
        return comp1 > comp2;
    };
    
    var greaterEqualsComp = function(val1, val2)
    {
        var comp1 = typeof val1 == 'undefined' ? null : val1;
        var comp2 = typeof val2 == 'undefined' ? null : val2;
        
        return comp1 >= comp2;
    };
    var lessComp = function(val1, val2)
    {
        var comp1 = typeof val1 == 'undefined' ? null : val1;
        var comp2 = typeof val2 == 'undefined' ? null : val2;
        
        return comp1 < comp2;
    };
    var lessEqualsComp = function(val1, val2)
    {
        var comp1 = typeof val1 == 'undefined' ? null : val1;
        var comp2 = typeof val2 == 'undefined' ? null : val2;
        
        return comp1 <= comp2;
    };
    
    var COMPARITOR_MAP = 
    {
        "="  : equalsComp,
        "!=" : notEqualsComp,
        ">"  : greaterComp,
        ">=" : greaterEqualsComp,
        "<"  : lessComp,
        "<=" : lessEqualsComp
    }

    ACE.AML.ControllerManager.register("extensions:CoverageApprovalsRowValidationController", CoverageApprovalsRowValidationController);
//# sourceURL=CoverageApprovalsRowValidationController.js
})(jQuery);