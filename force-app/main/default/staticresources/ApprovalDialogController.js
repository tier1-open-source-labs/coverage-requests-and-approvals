/**
 * Name: ApprovalDialogController.js 
 * Description: Handles the modal for confirming the selected action.
 *              This was required to be a separate dialog since requirements
 *              stated that there will be custom fields (such as reason) in the confirm dialog.
 *              This controller is dependent on the PendingRequestsConfirm controller.
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */

(function($)
{	
	function ApprovalDialogController(context)
	{
        var _this = this;
        this._context = context;
        _this.options = context.options;

        _this.parentController = _this._context.local.state.get('parentControl');

        //We have the parent controller in state. If the confirm
        this._context.local.eventDispatcher.on("processRecords", function(e, eventData)
        {
            //Call the parent controller, using it as the first parameter to preserve its context
            //Send the dialog state as the function parameter
            _this.parentController[_this.options.callback].call(_this.parentController, _this._context.local.state.data);
        });

        //We have the parent controller in state. If the confirm is closed, disable the lock
        this._context.local.eventDispatcher.on("close", function(e, eventData)
        {
            //Call the parent controller, using it as the first parameter to preserve its context
            //Send the dialog state as the function parameter
            _this.parentController._context.local.state.put('isSaving', false);
        });

        this._context.readyHandler();
    }

    ACE.AML.ControllerManager.register("extensions:approvalDialogController", ApprovalDialogController);
    
	//# sourceURL=ApprovalDialogController.js
})(jQuery);