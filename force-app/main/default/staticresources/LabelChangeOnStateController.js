/**
 * Name: LabelChangeOnStateController.js 
 * Description: 
 *
 * Confidential & Proprietary, 2020 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */

(function($)
{      
    var BASE_CONFIG = 
    { 
        mergeText : "Default Label ({propertyValue})",
        propertyValue : "pendingRequestsCount",
        defaultValue: "",
        domElement : ""
    }

    function LabelChangeOnStateController(context)
    {        
        var _this = this;
        _this._context = context;        
        _this.options = $.extend({}, BASE_CONFIG, context.options);
        
        _this._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e, eventData)
        {
            _this.changeLabel();            
        });

        _this._context.readyHandler();
    }
    
    LabelChangeOnStateController.prototype.init = function()
    { 
        var _this = this;
        _this.changeLabel();  
    }

    LabelChangeOnStateController.prototype.changeLabel = function()
    { 
        var _this = this;

        var countObj = 
        {
            'propertyValue' : _this._context.local.state.get(_this._context.options.propertyValue) || _this._context.options.defaultValue
        }
        
        var mergeText = _this.options.mergeText;    
        var label = ACE.StringUtil.mergeParameters(mergeText, countObj);
        
        var target = _this.options.domElement || _this._context.target;
        $(target).text(label);
    }

   
    ACE.AML.ControllerManager.register("extensions:labelChangeOnStateController", LabelChangeOnStateController);    
//# sourceURL=LabelChangeOnStateController.js
})(jQuery);