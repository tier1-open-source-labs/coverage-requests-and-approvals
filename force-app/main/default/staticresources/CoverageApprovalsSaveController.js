/**
 * Name: CoverageApprovalsSaveController.js 
 * Description: Controller that calls apex controller to upsert records for the
 *              coverage approvals form
 *
 * Confidential & Proprietary, 2020 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM. Test
 */

(function($)
{ 
    var BASE_CONFIG = 
    {
        dataProvider    : "coveragesMap",
        controllerClass : "CoverageApprovalManagerController.saveCoverages",
        //Field on the acai object to be cleaned up before saving
        deleteField     : "T1C_Base__Account_Coverage__r.Approval_External_Id__c",
        empIdField      : "employee.Id",
        batchSize       : 25
    }

    function CoverageApprovalsSaveController(context)
    {
        this._context = context;	
        var _this = this;
        _this.options = $.extend({}, BASE_CONFIG, context.options);
        this._context.readyHandler();
    }
    
    //Temporary until we convert to Locale files
    var BUNDLE = 
    {
        PROCESSING : 'Processing...'
    }

    ACE.Locale.registerBundle("en_US",BUNDLE);

    CoverageApprovalsSaveController.prototype.execute = function(e, eventData, callback)
    {
        var _this = this;

        //A simple latch to make sure we dont submit multiple times while already saving
        if( _this._context.local.state.get('isSaving') )
        {
            callback(false);
            return;
        }

        console.log('save request received');
        _this._context.local.state.put('isSaving', true);
        try
        {
            //We need to convert the map to a list
            // var coveragesMap = $.extend({}, _this._context.local.state.get(_this.options.dataProvider) );
            var coveragesMap = $.extend({}, JSON.parse(JSON.stringify(_this._context.local.state.get(_this.options.dataProvider))));
            var covsList = [];

            var covAIIds = []; // IDs of valid rows. 
            
            var hasInvalidRow = false; 

            for (var prop in coveragesMap) 
            {
                if (Object.prototype.hasOwnProperty.call(coveragesMap, prop)) 
                {
                    var covWrapper = coveragesMap[prop];

                    // loop through cov AIs, and remove it from the wrapper if its invalid. 
                    for (var i = covWrapper.covAIs.length -1; i >= 0; i--)
                    {
                        var acai = covWrapper.covAIs[i];
                        
                        //Clean up any bad fields if its valid. 
                        if (acai.invalid == null || !acai.invalid )
                        {
                            covAIIds.push(acai.id);
                            //delete acai[_this.options.deleteField];
                            delete acai._customIsSelected;
                            delete acai.invalid;
                            delete acai.id;
                        }
                        else 
                        {
                            hasInvalidRow = true; 
                            covWrapper.covAIs.splice(i, 1);
                        }
                    }

                    if (covWrapper.covAIs.length > 0)
                    {
                        //Get rid of the 'selected' grid property
                        delete covWrapper.selected;
                        covsList.push(covWrapper);
                    }
                }
            }; 


            if (hasInvalidRow)
            {   
                var footerCoveragesCount = _this._context.local.state.get('footerCoveragesCount');
                var warningString = _this._context.options.warningMessage;
                warningString = warningString.replace('{VALID_COV_NUM}', (covAIIds.length == null ? '0' : covAIIds.length ));
                warningString = warningString.replace('{TOTAL_COV_NUM}', footerCoveragesCount);

                var yesString = ACE.Locale.getBundleValue('YES');
                var noString = ACE.Locale.getBundleValue('NO');

                ACEConfirm.show
                (
                    warningString,
                    "Warning",
                    [ yesString + "::save-button confirmButton",  
                      noString + "::el-btn-lg el-btn-outline"],
                    function(result)
                    {
                        if(result == "YES")
                        {
                            _this.saveCoverage(covsList, covAIIds, callback)  
                        }
                        else
                        {
                            _this._context.local.state.put('isSaving', false);
                        }
                    }
                );  
            }
            else 
            {
                _this.saveCoverage(covsList, covAIIds, callback)  
            }
        }catch(e)
        {
            _this._context.local.state.put('isSaving', false);
        }
    }

    CoverageApprovalsSaveController.prototype.saveCoverage = function(covsList, covAIIds, callback)
    {
        var _this = this; 
        var empId = _this._context.local.state.get(_this.options.empIdField);

        if(covAIIds.length < 1)
        {
            _this._context.local.state.put('isSaving', false);
            return;
        }

        var batch = parseInt(_this.options.batchSize);
        batch = batch > 0 ? batch : 25;


        ACE.Remoting.batch( _this.options.controllerClass, covsList, function(args)
        {
            return [args,empId];
        }, 
        function (result, event)
        {
            _this._context.local.state.put('isSaving', false);
            if (event)
            {			
                console.log('Records Upserted');
                _this._context.local.eventDispatcher.trigger('coverageFinishedSaving', {"covAIIds": covAIIds});
                callback(true);   
            }
            else
            {
                //Error handler here
                callback(false);   
            }								
        },
        {
            retryOnTimeout : false,
            batchSize : batch,
            showProgressBar : true,
            alwaysShowProgressBar : true
        },
        {
            retryOnTimeout : false,
            batchSize : batch,
            showProgressBar : true,
            alwaysShowProgressBar : true
        });    
    }

    
    ACE.AML.ControllerManager.register("extensions:coverageApprovalsSaveController", CoverageApprovalsSaveController);
//# sourceURL=CoverageApprovalsSaveController.js
})(jQuery);