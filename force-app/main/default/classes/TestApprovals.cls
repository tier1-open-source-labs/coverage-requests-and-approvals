@isTest
public with sharing class TestApprovals
{
    @testSetup static void testConfigs() 
    {
        //Setup the AFRs
        //Create AFRs: ACE.Extensions.AccountCoverageApprovals.Settings
		T1C_FR__Feature__c aceFeature                = new T1C_FR__Feature__c(Name = 'ACE',                       T1C_FR__Name__c = 'ACE');
		T1C_FR__Feature__c extensions                = new T1C_FR__Feature__c(Name = 'Extensions',                T1C_FR__Name__c = 'ACE.Extensions');
        T1C_FR__Feature__c AccountCoverageApprovals  = new T1C_FR__Feature__c(Name = 'AccountCoverageApprovals',  T1C_FR__Name__c = 'ACE.Extensions.AccountCoverageApprovals');
        T1C_FR__Feature__c Settings                  = new T1C_FR__Feature__c(Name = 'Settings',                  T1C_FR__Name__c = 'ACE.Extensions.AccountCoverageApprovals.Settings');

        T1C_FR__Feature__c requester   = new T1C_FR__Feature__c(Name = 'Settings',    T1C_FR__Name__c = 'ACE.Extensions.AccountCoverageApprovals.Settings.Requester');
        T1C_FR__Feature__c requestsTab = new T1C_FR__Feature__c(Name = 'RequestsTab', T1C_FR__Name__c = 'ACE.Extensions.AccountCoverageApprovals.Settings.Requester.RequestsTab');
        T1C_FR__Feature__c reqGridCols = new T1C_FR__Feature__c(Name = 'GridColumns', T1C_FR__Name__c = 'ACE.Extensions.AccountCoverageApprovals.Settings.Requester.RequestsTab.GridColumns');
        T1C_FR__Feature__c nameCol     = new T1C_FR__Feature__c(Name = 'Name',        T1C_FR__Name__c = 'ACE.Extensions.AccountCoverageApprovals.Settings.Requester.RequestsTab.GridColumns.Name');

        T1C_FR__Feature__c pendingTab  = new T1C_FR__Feature__c(Name = 'PendingTab',         T1C_FR__Name__c = 'ACE.Extensions.AccountCoverageApprovals.Settings.Requester.PendingTab');
        T1C_FR__Feature__c pendingTabG = new T1C_FR__Feature__c(Name = 'GridColumns',         T1C_FR__Name__c = 'ACE.Extensions.AccountCoverageApprovals.Settings.Requester.PendingTab.GridColumns');
        

        T1C_FR__Feature__c covhistTab  = new T1C_FR__Feature__c(Name = 'CoverageHistoryTab', T1C_FR__Name__c = 'ACE.Extensions.AccountCoverageApprovals.Settings.Requester.CoverageHistoryTab');
        T1C_FR__Feature__c covhistTabG = new T1C_FR__Feature__c(Name = 'GridColumns', T1C_FR__Name__c = 'ACE.Extensions.AccountCoverageApprovals.Settings.Requester.CoverageHistoryTab.GridColumns');
        

        T1C_FR__Feature__c expiredTab  = new T1C_FR__Feature__c(Name = 'ExpiredCoverageTab', T1C_FR__Name__c = 'ACE.Extensions.AccountCoverageApprovals.Settings.Requester.ExpiredCoverageTab');
        T1C_FR__Feature__c expiredTabG = new T1C_FR__Feature__c(Name = 'GridColumns', T1C_FR__Name__c = 'ACE.Extensions.AccountCoverageApprovals.Settings.Requester.ExpiredCoverageTab.GridColumns');
        

        /*PendingTab
        CoverageHistoryTab
        ExpiredCoverageTab*/


        T1C_FR__Feature__c dupeRules   = new T1C_FR__Feature__c(Name = 'DuplicateRules', T1C_FR__Name__c = 'ACE.Extensions.AccountCoverageApprovals.Settings.DuplicateRules');
        T1C_FR__Feature__c productRule = new T1C_FR__Feature__c(Name = 'ProductRule', T1C_FR__Name__c = 'ACE.Extensions.AccountCoverageApprovals.Settings.DuplicateRules.ProductRule');

        insert new List<T1C_FR__Feature__c>{aceFeature, extensions, accountCoverageApprovals, settings, 
            requester, requestsTab, reqGridCols, nameCol, pendingTab, covhistTab, expiredTab, dupeRules, productRule, pendingTabG, covhistTabG, expiredTabG};

        List<T1C_FR__Attribute__c> attributes = new  List<T1C_FR__Attribute__c>();

        attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = nameCol.Id,Name = 'Label',     T1C_FR__Value__c = 'Name', T1C_FR__User_Overridable__c = true));
		attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = nameCol.Id,Name = 'Order',     T1C_FR__Value__c = '7', T1C_FR__User_Overridable__c = true));
		attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = nameCol.Id,Name = 'Visible',   T1C_FR__Value__c = 'True', T1C_FR__User_Overridable__c = true));
		attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = nameCol.Id,Name = 'DataField', T1C_FR__Value__c = 'Name', T1C_FR__User_Overridable__c = true));
		attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = nameCol.Id,Name = 'FieldType', T1C_FR__Value__c = 'STRING', T1C_FR__User_Overridable__c = true));
        attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = nameCol.Id,Name = 'Width',     T1C_FR__Value__c = '120', T1C_FR__User_Overridable__c = true));

        attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = dupeRules.Id,Name = 'MatchFields', T1C_FR__User_Overridable__c = true, 
            T1C_FR__Value__c = 'T1C_Base__Account_Coverage__r.T1C_Base__Account__c,T1C_Base__Account_Coverage__r.T1C_Base__Employee__c'));
        
        attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = dupeRules.Id,Name = 'Message', T1C_FR__User_Overridable__c = true, 
            T1C_FR__Value__c = 'You are already requesting this coverage with a conflicting role.'));

        attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = productRule.Id,Name = 'MatchFields', T1C_FR__User_Overridable__c = true, 
            T1C_FR__Value__c = 'T1C_Base__Product__c,T1C_Base__Region__c,T1C_Base__Account_Coverage__r.T1C_Base__Account__c'));
        
        attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = productRule.Id,Name = 'Message', T1C_FR__User_Overridable__c = true, 
            T1C_FR__Value__c = 'You are already requesting this coverage with a conflicting role.'));
            
        attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = productRule.Id,Name = 'RestrictedRoles', T1C_FR__User_Overridable__c = true, 
            T1C_FR__Value__c = 'testRole'));

        insert attributes;
    }

    static testMethod void myUnitTest() 
    {
        Tier1CRMTestDataFactoryCore testFactory = new Tier1CRMTestDataFactoryCore();

        Account testAcnt = new Account(Name = 'QuackTest');

        insert testAcnt;

        Profile prof = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        //Make a Coverage Manager User
        User manager = new User(Alias             = 'manager', 
                                Email             = 'manager@testorg.com', 
                                EmailEncodingKey  = 'UTF-8', 
                                LastName          = 'Manager', 
                                LanguageLocaleKey = 'en_US', 
                                LocaleSidKey      = 'en_US', 
                                ProfileId         =  prof.Id, 
                                TimeZoneSidKey    = 'America/Los_Angeles', 
                                UserName          = 'managingUser@testorg.com',
                                IsActive          = true); 

        //Note this will count against number of licenses if the insert keyword is used
        System.runAs(manager){}

        //Make a Coverage Manager User
        User normalUser = new User(Alias          = 'normGuy', 
                                Email             = 'norm@testorg.com', 
                                EmailEncodingKey  = 'UTF-8', 
                                LastName          = 'Guy', 
                                LanguageLocaleKey = 'en_US', 
                                LocaleSidKey      = 'en_US', 
                                ProfileId         =  prof.Id, 
                                TimeZoneSidKey    = 'America/Los_Angeles', 
                                UserName          = 'normalUser@testorg.com',
                                DelegatedApproverId = manager.Id,
                                ManagerId = manager.Id);

        System.runAs(normalUser){}

        //Create an example LOB
        T1C_Base__Organization_Structure__c newOrgStruct = new T1C_Base__Organization_Structure__c(Name='Test Struct');
        insert newOrgStruct;

        //Make current User as an employee
        T1C_Base__Employee__c userEmp = testFactory.makeEmployee('User', 'Guy', normalUser.Id);
        userEmp.T1C_Base__LOB__c = newOrgStruct.Id;
        userEmp.T1_Coverage_Approver__c = manager.Id;
        insert userEmp;

        //Make current Manager as an employee
        T1C_Base__Employee__c managerEmp = testFactory.makeEmployee('Manager', 'Guy', manager.Id);
        //userEmp.T1_Coverage_Manager__c = manager.Id;
        insert managerEmp;

        //Make an example product and region
        T1C_Base__Product__c newProd = new T1C_Base__Product__c(T1C_Base__LOB__c=newOrgStruct.Id);
        upsert newProd;

        T1C_Base__Region__c newRegion = new T1C_Base__Region__c(Name='Global');
        upsert newRegion;

        //Test fetching the configs
        List<String> accountIds = new List<String>{(String)testAcnt.Id};
            CoverageApprovalManagerController.getState('Requester', normalUser.Id, accountIds);

        T1C_Base__Account_Coverage__c cov = testFactory.makeAccountCoverage(testAcnt.Id, userEmp.Id);
        cov.Approval_External_Id__c = (String)testAcnt.Id + (String)userEmp.Id;

        List<T1C_Base__Account_Coverage_Additional_Info__c> acais = new List<T1C_Base__Account_Coverage_Additional_Info__c>();

        acais.add( new T1C_Base__Account_Coverage_Additional_Info__c(T1C_Base__End_Date__c = system.today(), T1C_Base__Role__c='testRole' ));
        acais.add( new T1C_Base__Account_Coverage_Additional_Info__c(T1C_Base__End_Date__c = system.today(), 
            T1C_Base__Role__c='testRole2', T1C_Base__Product__c=newProd.Id) );

        CoverageApprovalInterfaces.CoverageFlatWrapper flatWrap = 
            new CoverageApprovalInterfaces.CoverageFlatWrapper();

        flatWrap.covRecord = cov;
        flatWrap.covAIs = acais;
        flatWrap.covType = 'T1C_Base__Account_Coverage__c';
        flatWrap.infoType = 'T1C_Base__Account_Coverage_Additional_Info__c';

        List<CoverageApprovalInterfaces.CoverageFlatWrapper> covList = 
            new List<CoverageApprovalInterfaces.CoverageFlatWrapper>();

        covList.add(flatWrap);

        String covsJSON = JSON.serialize(covList);

        CoverageApprovalManagerController.saveDraft(userEmp.Id, new List<String>{covsJSON});

        try
        {
            CoverageApprovalManagerController.saveCoverages(covList, userEmp.Id);
            CoverageApprovalManagerController.renotifyApprover(userEmp.Id, 'T1C_Base__Account_Coverage_Additional_Info__c');
        }
        catch(Exception e)
        {
            System.debug('Skipping Email Deliverability test due to errors: ' + e.getMessage());
        }
        //system.runAs(user, block)

        List<T1C_Base__Account_Coverage_Additional_Info__c> logList = 
            [SELECT Id, Name, T1C_Base__Account_Coverage__c, T1C_Base__Start_Date__c, T1C_Base__End_Date__c FROM T1C_Base__Account_Coverage_Additional_Info__c];

        system.debug('Final List: ' + logList);

        List<ProcessInstance> procs = [SELECT Id,
                                              TargetObjectId,
                                              TargetObject.Type, 
                                              Status,
                                              (SELECT Id, ActorId, ProcessInstanceId FROM Workitems)
                                         FROM ProcessInstance];
                                        

        for(ProcessInstance proc : procs)
        {
            system.debug(proc);
        }

        List<String> queryFields = new List<String>{'Id', 'Name', 'T1C_Base__Account_Coverage__c'};

        List<Id> listPendingRequestIds = new List<Id>{logList[1].Id};

        CoverageApprovalManagerController.cancelPendingRequests(listPendingRequestIds);

        CoverageApprovalManagerController.getExpiredRecords(
            UserInfo.getUserId(), 'T1C_Base__Account_Coverage_Additional_Info__c', new List<String>{'Id', 'Name'}, '', 'String viewType');
        CoverageApprovalManagerController.getHistoryRecords(
            UserInfo.getUserId(), 'T1C_Base__Account_Coverage_Additional_Info__c', new List<String>{'Id', 'Name'}, '', 'String viewType');

        System.runAs(manager) 
        {
            //List<SObject> approveIds = new List<SObject>();
            List<SObject> resList = CoverageApprovalManagerController.getPendingApprovalRecords(UserInfo.getUserId(), null,
                'T1C_Base__Account_Coverage_Additional_Info__c', queryFields, '', 'Approver');
        
            List<Id> approveList = new List<Id>();
            for(SObject obj : resList)
            {
                approveList.add( (Id)obj.get('Id') );
            }
        
            try
            {
                CoverageApprovalManagerController.processRecords(approveList, 'Approve', UserInfo.getUserId(), null);
            }
            catch(Exception e)
            {
                System.debug('Skipping Email Deliverability test due to errors: ' + e.getMessage());
            }
        }

        
        
        /*
        saveDraft
        getClientHierarchy
        getExpiredRecords
        getHistoryRecords

        PendingTab
        CoverageHistoryTab
        ExpiredCoverageTab
        */
    }

    static testMethod void testDupes() 
    {
        Tier1CRMTestDataFactoryCore testFactory = new Tier1CRMTestDataFactoryCore();

        T1C_Base__Employee__c userEmp = testFactory.makeLoggedInUserEmployee();
        insert userEmp;

        Account testAcnt = new Account(Name = 'QuackTest');

        insert testAcnt;

        T1C_Base__Account_Coverage__c cov = testFactory.makeAccountCoverage(testAcnt.Id, userEmp.Id);
        cov.Approval_External_Id__c = (String)testAcnt.Id + (String)userEmp.Id;

        insert cov;
        
        T1C_Base__Account_Coverage_Additional_Info__c existingCov =
            new T1C_Base__Account_Coverage_Additional_Info__c(T1C_Base__End_Date__c = system.today(), T1C_Base__Role__c='testRole',T1C_Base__Account_Coverage__c=cov.Id );

        insert existingCov;

        Map<String, SObject> covsMap = new Map<String, SObject>
        {
            'newRow1' => new T1C_Base__Account_Coverage_Additional_Info__c(T1C_Base__End_Date__c = system.today(), T1C_Base__Role__c='testRole',T1C_Base__Account_Coverage__r=cov ),
            'newRow2' => new T1C_Base__Account_Coverage_Additional_Info__c(T1C_Base__End_Date__c = system.today(), T1C_Base__Role__c='testRole',T1C_Base__Account_Coverage__r=cov )
        };
        
        CoverageApprovalsTriggerHelper.checkDupes(covsMap);
    }
}