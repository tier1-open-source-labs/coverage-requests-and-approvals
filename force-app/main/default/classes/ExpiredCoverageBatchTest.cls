/*
 * Name:    ExpiredCoverageBatchTest
 *
 * Confidential & Proprietary, 2020 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial Solutions Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1 Financial Solutions Inc.
 */

@isTest

public with sharing class ExpiredCoverageBatchTest 
{
    private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

    private @IsTest
    static void expiredBatchTest() 
    {
        dataFactory.createCBSAFR();

        T1C_FR__Feature__c ACESecurity = new T1C_FR__Feature__c(Name = 'Security', T1C_FR__Name__c = 'ACE.Security');
        T1C_FR__Feature__c ACESecurityDisabled = new T1C_FR__Feature__c(Name = 'DisabledTriggers', T1C_FR__Name__c = 'ACE.Security.DisabledTriggers');
        T1C_FR__Feature__c ACEExtensions = new T1C_FR__Feature__c(Name = 'Extensions', T1C_FR__Name__c = 'ACE.Extensions');
        T1C_FR__Feature__c ExpiredCoverageBatch = new T1C_FR__Feature__c(Name = 'ExpiredCoverageBatch', T1C_FR__Name__c = 'ACE.Extensions.ExpiredCoverageBatch');
        
        insert new list<T1C_FR__Feature__c>{ACESecurity,ACESecurityDisabled,ACEExtensions,ExpiredCoverageBatch};

        insert new list<T1C_FR__Attribute__c>{
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACESecurityDisabled.Id, Name = 'MIGRATION', T1C_FR__Value__c = 'true'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ExpiredCoverageBatch.Id, Name = 'ImagePlaceHolder', T1C_FR__Value__c = 'www.google.com'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ExpiredCoverageBatch.Id, Name = 'HeaderHTML', T1C_FR__Value__c = '<body>'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ExpiredCoverageBatch.Id, Name = 'FooterHTML', T1C_FR__Value__c = '</body')
        };

        T1C_Base__Employee__c otherEmp = dataFactory.makeEmployee('Marla', 'Singer', null);
        otherEmp.T1C_Base__Title__c = 'Soap R&D';
        otherEmp.T1_Coverage_Approver__c = UserInfo.getUserId();
        insert otherEmp;

        Account acct = dataFactory.makeAccount('Paper Street Soap Company');
        insert acct;

        T1C_Base__Account_Coverage__c ac = dataFactory.makeAccountCoverage(acct.Id, otherEmp.Id);
        insert ac;

        T1C_Base__Product__c product = dataFactory.makeProduct('Soap', null);
        insert product;

        T1C_Base__Region__c region = new T1C_Base__Region__c(Name = 'Canada');
        insert region;

        T1C_Base__Account_Coverage_Additional_Info__c acai = new T1C_Base__Account_Coverage_Additional_Info__c(T1C_Base__Account_Coverage__c = ac.Id, 
                                                                                                                T1C_Base__Role__c = 'Primary', 
                                                                                                                T1C_Base__Product__c = product.Id,
                                                                                                                T1C_Base__Region__c = region.Id,
                                                                                                                T1C_Base__End_Date__c = Date.today());
        insert acai;

        Test.startTest();
        Database.executeBatch(new ExpiredCoverageBatch(), 1);
        Test.stopTest();
    }
}