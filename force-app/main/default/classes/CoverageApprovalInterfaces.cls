/**
 * Name: CoverageApprovalInterfaces
 * Description: Definitions class for the coverage approval manager. 
 *              Contains all interfaces for the back end
 *
 * Confidential & Proprietary, 2015 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 */
public with sharing class CoverageApprovalInterfaces 
{
    public static final String COVERAGE_CONFIG_PATH = 'ACE.Extensions.AccountCoverageApprovals.Settings';

    public interface CoverageFlatObject 
    {
        List<SObject> getAdditionalInfos();
        void setData( CoverageFlatWrapper covWrapper);
    }

    public class AccountCoverageDataProcessor implements CoverageFlatObject
    {
        public List<T1C_Base__Account_Coverage_Additional_Info__c> acais;

        public AccountCoverageDataProcessor()
        {}

        public List<T1C_Base__Account_Coverage_Additional_Info__c> getAdditionalInfos()
        {
            return this.acais;
        }

        public void setData(CoverageFlatWrapper covWrapper)
        {
            this.setAdditionalInfos( covWrapper.covAIs );

            //Set the end dates so that these won't appear in the cubes and other managers
            //system.debug();
            this.setEndDates();//Date.today().addDays(-1) );
            this.setParents(covWrapper.covRecord);
        }

        public void setAdditionalInfos(List<SObject> infos)
        {
            this.acais = infos;
            for(SObject obj : this.acais)
            {
                obj.put('Approval_Status__c', 'Pending');
            }
        }

        public void setEndDates()
        {
            for(T1C_Base__Account_Coverage_Additional_Info__c curAI : this.acais)
            {
                DateTime dTS = DateTime.valueOf(0);
                Date startDate = Date.newinstance(dTS.year(), dTS.month(), dTS.day());

                DateTime dTE = DateTime.valueOf(30);
                Date endDate = Date.newinstance(dTE.year(), dTE.month(), dTE.day());


                curAI.T1C_Base__Start_Date__c = startDate;
                curAI.T1C_Base__End_Date__c = endDate;
            }
        }

        public void setParents(SObject parent)
        {
            for(T1C_Base__Account_Coverage_Additional_Info__c curAI : this.acais)
            {
                curAI.T1C_Base__Account_Coverage__r = 
                    new T1C_Base__Account_Coverage__c(Approval_External_Id__c = (String)parent.get('Approval_External_Id__c') );
            }
        }
    }

    //Need this kind of interface because of the External Id upsert restrictions 
    //that SF enforces
    public interface CoverageInserter 
    {
        void pushToList(SObject data);
        List<SObject> getDataList();
        //Expected behaviour when the list should be auto approved
        //Made this because the auto approved records dont actually go through the approval process
        void autoApproveFieldUpdate();
        void upsertList();
    }

    public class AccountCoverageDataInserter implements CoverageInserter
    {
        private Map<String, T1C_Base__Account_Coverage__c> covsList = new Map<String, T1C_Base__Account_Coverage__c>();
        public void pushToList(SObject data)
        {
            System.debug('Found AC Record with External Id: ' + data.get('Approval_External_Id__c'));
            //Unique Id needs to be defined before upserting
            //uniqueId = ac.Account__c + ':' + ac.Employee__c + ':' + ac.Product__c + ':' + ac.Start_Date__c + ':' + ac.End_Date__c + ':' +ac.Product_Lookup__c;
            String uniqueId = data.get('T1C_Base__Account__c') + ':' 
            + data.get('T1C_Base__Employee__c') + ':' 
            + data.get('T1C_Base__Product__c') + ':' 
            + 'null:'
            + 'null:'
            + data.get('T1C_Base__Product_Lookup__c');

            data.put('T1C_Base__Unique_Id__c', uniqueId);

            //set the dates so that the record is inactive
            DateTime dTS = DateTime.valueOf(0);
            Date startDate = Date.newinstance(dTS.year(), dTS.month(), dTS.day());

            DateTime dTE = DateTime.valueOf(30);
            Date endDate = Date.newinstance(dTE.year(), dTE.month(), dTE.day());

            data.put('T1C_Base__Start_Date__c', startDate); 
            data.put('T1C_Base__End_Date__c', endDate);

            this.covsList.put((String)data.get('Approval_External_Id__c'), (T1C_Base__Account_Coverage__c) data);
        }

        public void upsertList()
        {
            //Scan for the previous coverages. If the coverage is already saved and is Active, 
            //we should null out the end dates
            List<String> externalIds = new List<String>();
            for(T1C_Base__Account_Coverage__c curCov : covsList.values())
            {
                externalIds.add( curCov.Approval_External_Id__c );
            }

            for(T1C_Base__Account_Coverage__c existingCov : [SELECT Id,
                                                                    Approval_External_Id__c
                                                               FROM T1C_Base__Account_Coverage__c
                                                              WHERE Approval_External_Id__c IN :externalIds])
            {
                T1C_Base__Account_Coverage__c newCov = this.covsList.get(existingCov.Approval_External_Id__c);

                if(newCov != null)
                {
                    this.covsList.remove(newCov.Approval_External_Id__c);
                }

            }


            if(covsList.size() > 0)
            {
                upsert this.covsList.values() Approval_External_Id__c;
            }
        }

        public void autoApproveFieldUpdate()
        {

        }

        public List<T1C_Base__Account_Coverage__c> getDataList()
        {
            return this.covsList.values();
        }
    }

    public class AccountCoverageAdditionInfoDataInserter implements CoverageInserter
    {
        private List<T1C_Base__Account_Coverage_Additional_Info__c> covsList = new List<T1C_Base__Account_Coverage_Additional_Info__c>();
        public void pushToList(SObject data)
        {
            this.covsList.add( (T1C_Base__Account_Coverage_Additional_Info__c) data);
        }

        public void upsertList()
        {
            if(covsList.size() > 0)
            {
                upsert this.covsList;
            }
        }

        public void autoApproveFieldUpdate()
        {
            for(T1C_Base__Account_Coverage_Additional_Info__c cov : this.covsList)
            {
                cov.T1C_Base__End_Date__c = cov.Requested_End_Date__c;
                cov.Approval_Status__c = 'Approved';
            }
        }

        public List<T1C_Base__Account_Coverage_Additional_Info__c> getDataList()
        {
            return this.covsList;
        }
    }

    public class CoverageManagerConfig
    {
        //Other configs here i guess

        //Map of Tab name to grid columns
        //Key -> Tab name string
        //Value -> list of grid configs
        public Map<String, List<CoverageManagerGridConfig>> gridCols;

        public CoverageManagerConfig(T1C_FR.Feature rootFeature)
        {
            this.gridCols  = new Map<String, List<CoverageManagerGridConfig>>();

            for(T1C_FR.Feature subFeat : rootFeature.subFeatures)
            {
                this.gridCols.put( subFeat.getName(), getConfigsForGrid(subFeat) );
            }
        }

        private List<CoverageManagerGridConfig> getConfigsForGrid(T1C_FR.Feature gridFeature)
        {
            T1C_FR.Feature colsFeature = gridFeature.getFeature('GridColumns');

            if(colsFeature == null)
            {
                return null;
            }
            
            List<CoverageManagerGridConfig> gridCols = new List<CoverageManagerGridConfig>();

            for(T1C_FR.Feature colFeat : colsFeature.subFeatures)
            {
                gridCols.add(new CoverageManagerGridConfig(colFeat));
            }

            gridCols.sort();

            return gridCols;
        }

    }

    public class CoverageManagerGridConfig implements Comparable
    {
        public String  id;
        public String  fieldType;
        public String  field; 
        public Integer minWidth;
        public String  name;
        public Integer order; 
        public Boolean editable;
        public Boolean visible; 
        public String  defaultValue;
        public String  stateProvider;
        public String  listDataField;
        public String  labelField;
        public Object editorOptions;

        public CoverageManagerGridConfig(T1C_FR.Feature inputFeature)
        {            
            this.id            = inputFeature.getName();
            this.fieldType     = inputFeature.getAttributeValue('FieldType');
            this.field         = inputFeature.getAttributeValue('DataField');
            this.name          = inputFeature.getAttributeValue('Label');
            this.editable      = Boolean.valueOf( inputFeature.getAttributeValue('Editable', 'True') );
            this.visible       = Boolean.valueOf( inputFeature.getAttributeValue('Visible') );
            this.defaultValue  = inputFeature.getAttributeValue('DefaultValue');
            this.stateProvider = inputFeature.getAttributeValue('StateValue');
            this.labelField    = inputFeature.getAttributeValue('LabelField');
            this.listDataField = inputFeature.getAttributeValue('ListDataField');
            String wid = inputFeature.getAttributeValue('Width');
            this.minWidth = wid.length() > 0 ? Integer.valueOf( wid ) : 0; 
            String ord = inputFeature.getAttributeValue('Order');
            this.order = ord.length() > 0 ? Integer.valueOf( ord ) : 0;

            //Additional Info
            if( this.fieldType.toLowerCase() == 'lookup' )
            {
                SObjectUtil.DynamicUILookupConfig o = 
                    new SObjectUtil.DynamicUILookupConfig( inputFeature.SubFeatMap.get(inputFeature.Name + '.FieldConfig') );
                this.editorOptions = new SObjectUtil.DynamicUILookupConfig();
                this.editorOptions = o;
            }//field type LOOKUP
            else if( this.fieldType.toLowerCase() == 'picklist' )
            {
                String settingsFeatName = CoverageApprovalInterfaces.COVERAGE_CONFIG_PATH;
                String pickListSobject = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), settingsFeatName, 'SObjectType');

                SObjectUtil.DynamicUIPicklistConfig o = new SObjectUtil.DynamicUIPicklistConfig();
                list<SObjectUtil.ComboItem> pickListValues = o.getPicklistValues(pickListSobject, this.field, true);
                o.dataProvider = pickListValues;

                //instantiate additonalConfig as DynamicUIPicklistConfig
                this.editorOptions = new SObjectUtil.DynamicUIPicklistConfig();
                this.editorOptions = o;
            }
        }

        public Integer compareTo(Object compareTo)
		{
			CoverageManagerGridConfig compareToCol = (CoverageManagerGridConfig)compareTo;
			if (order == compareToCol.order) return 0;
			if (order > compareToCol.order) return 1;
			return -1;
		}
    }

    public class CoverageFlatWrapper
    {
        public T1C_Base__Account_Coverage__c covRecord;//Make it so that this works with generic SObject to pave the way for CCAIs
        public List <SObject> covAIs;
        public Boolean revoke;
        public String covType;//Account Coverage or Contact coverage
        public String infoType;//ACAI or CCAI

        public CoverageFlatWrapper()
        {

        }
    }

    public class CoverageException extends Exception
    {
        
    }

    public class ComboItem
    {
        public String id;
        public String data;
        public String label;

        public ComboItem(SObject obj)
        {
            this.id = (String)obj.get('Id');
            this.data = (String)obj.get('Id');
            this.label = (String)obj.get('Name');
        }

        public ComboItem()
        {

        }
    }
}