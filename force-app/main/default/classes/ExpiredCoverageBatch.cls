/*
 * Name:    ExpiredCoverageBatch
 *
 * Confidential & Proprietary, 2020 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial Solutions Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1 Financial Solutions Inc.
 */

global inherited sharing class ExpiredCoverageBatch implements Schedulable, Database.Batchable<SObject>, Database.Stateful 
{
    private set<Id> userIds = new set<Id>();
    private String headerHTML = '';
    private String footerHTML = '';
    private String subject;
    private String openingLine;
    private String empNameStyle;
    private String spacerDiv;
    private String lineBreakDiv;
    private String imagePlaceHolderUrl;
    private list<String> empIdentityFields = new list<String>{'T1C_Base__Title__c','T1C_Base__Department__c'};
    private set<String> identFields = new set<String>{'T1C_Base__Title__c','T1C_Base__Department__c'};
    private set<String> recordTypeNames = new set<String>{'Standard'};
    private Integer daysBack = 0;// Zero means today, 1 means yesterday - has to match the end date exactly
    private static final String CONFIG_PATH = CoverageApprovalManagerController.COVERAGE_CONFIG_PATH;


    //This field on the Employee object is assumed to be a checkbox field. Defaults to managed package inactive field
    public static String approverEmailOptOutField
    {
        set
        {

        }
        get
        {
            if(approverEmailOptOutField == null)
            {
                approverEmailOptOutField = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), CONFIG_PATH, 'ApproverEmailOptOutField');
                approverEmailOptOutField = approverEmailOptOutField != null && approverEmailOptOutField.length() > 0 ? approverEmailOptOutField : 'T1C_Base__Inactive__c';
            }
            return approverEmailOptOutField;
        }
    }

    global ExpiredCoverageBatch() 
    {
        T1C_FR.Feature configFeature = T1C_FR.featureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), CONFIG_PATH);
         
        if(configFeature != null)
        {
            recordTypeNames = new set<String>();

            daysBack = configFeature.getIntegerAttributeValue('ExpiredDaysBack', daysBack);
            recordTypeNames.addAll(configFeature.getAttributeValue('ACAIRecordTypes','Standard').split(';'));
            headerHTML = configFeature.getAttributeValue('HeaderHTML','');
            footerHTML = configFeature.getAttributeValue('FooterHTML','');
            empNameStyle = configFeature.getAttributeValue('EmployeeNameStyle','font-weight:700;color:#2567ca;font-size:1em');
            spacerDiv = configFeature.getAttributeValue('SpacerDiv','<div></div>');
            lineBreakDiv = configFeature.getAttributeValue('LineBreakDiv','<div style="height:15px"></div>');
            subject = configFeature.getAttributeValue('EmailSubject','Daily Client Coverage Expiry Notification for ');
            String dateFormat = configFeature.getAttributeValue('DateFormat','MMM d yyyy');
            openingLine = configFeature.getAttributeValue('OpeningLine','The following employees have <u>expired</u> their coverage as of ' + DateTime.now().format(dateFormat) + ':');
            imagePlaceHolderUrl = configFeature.getAttributeValue('ImagePlaceHolder','');
            
            list<String> identityFields = configFeature.getAttributeValue('IdentityFields','T1C_Base__Title__c;T1C_Base__Department__c').split(';');
            
            empIdentityFields = new list<String>();

            if(!identityFields.isEmpty())
            {
                identFields = new set<String>();
                
                for(String field : identityFields)
                {
                    if(identFields.add(field))
                    {
                        empIdentityFields.add(field);
                    }
                }
            }
        }

        Date expireDate = Date.today().addDays(daysBack *-1);

        //Query for the Employees that have opted out of the email notifications
        List<Id> userExcludeList = new List<Id>();
        String empQuery = 'SELECT Id, ' + 
                                 'T1C_Base__User__c, ' +
                                  approverEmailOptOutField +
                           ' FROM T1C_Base__Employee__c ' + 
                          ' WHERE ' + approverEmailOptOutField + ' = true';
        List<String> appFields = CoverageApprovalManagerController.empApproverField.split('\\.');

        for(T1C_Base__Employee__c emp : (List<T1C_Base__Employee__c>)Database.query(empQuery))
        {
            userExcludeList.add(emp.T1C_Base__User__c);
        }

        String acaiQry = 'select T1C_Base__Account_Coverage__r.T1C_Base__Employee__r.' + CoverageApprovalManagerController.empApproverField +
                            ' from T1C_Base__Account_Coverage_Additional_Info__c '+
                            ' where '+
                            ' (T1C_Base__Account_Coverage__r.T1C_Base__End_Date__c = :expireDate or T1C_Base__End_Date__c = :expireDate)'+
                            ' and RecordType.Name in :recordTypeNames'+
                            ' and T1C_Base__Account_Coverage__r.T1C_Base__Employee__r.'  + CoverageApprovalManagerController.empApproverField + ' != null';

        String approverFieldFromACAI = 'T1C_Base__Account_Coverage__r.T1C_Base__Employee__r.'  + CoverageApprovalManagerController.empApproverField;

        for(T1C_Base__Account_Coverage_Additional_Info__c acai : (List<T1C_Base__Account_Coverage_Additional_Info__c>) Database.query(acaiQry))
        {
            //Decided to do the loop instead of using a NOT IN clause to avoid the table scan that would happen
            //A table scan would be very slow for a table as big as the ACAI records
            Id approverUserId = (Id)CoverageApprovalsTriggerHelper.getFieldValue(acai, approverFieldFromACAI);

            //If the User Id is in the opt-out list
            if( userExcludeList.contains(approverUserId) )
            {
                return;
            }

            userIds.add(approverUserId);
        }        
    }

    global void execute(SchedulableContext sc)
	{
		Database.executeBatch(new ExpiredCoverageBatch(), 1);
    }
    
    global Database.QueryLocator start(Database.BatchableContext context)  
	{ 
        return Database.getQueryLocator('Select Id, Name from User where Id in :userIds'); 
    }

    global void execute(Database.BatchableContext context, List<SObject> scope)  
 	{
        Date expireDate = Date.today().addDays(daysBack *-1);
        String emailBody = '';
        map<Id, User> userMap = new map<Id, User>();
        map<Id, T1C_Base__Employee__c> empDataMap = new map<Id, T1C_Base__Employee__c>();
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();

        for(User u : (list<User>)scope)
        {
            userMap.put(u.Id, u);

            Set<Id> approverIds = userMap.keySet();

            String acaiQueryInner = 'select T1C_Base__Account_Coverage__r.T1C_Base__Account__c,' +
                                            'T1C_Base__Account_Coverage__r.T1C_Base__Account__r.Name,' +
                                            'T1C_Base__Account_Coverage__r.T1C_Base__Employee__c,' +
                                            'T1C_Base__Account_Coverage__c,' +
                                            'T1C_Base__Product__c, T1C_Base__Product__r.Name,' +
                                            'T1C_Base__Region__c, T1C_Base__Region__r.Name,' +
                                            'T1C_Base__Role__c,' +
                                            //.T1C_Base__Employee__r.T1_Coverage_Approver__c
                                            'T1C_Base__Account_Coverage__r.T1C_Base__Employee__r.' + CoverageApprovalManagerController.empApproverField + 
                                        ' from T1C_Base__Account_Coverage_Additional_Info__c ' +
                                        ' where ' +
                                            ' ((T1C_Base__Account_Coverage__r.T1C_Base__End_Date__c = :expireDate AND T1C_Base__End_Date__c = :expireDate) or T1C_Base__End_Date__c = :expireDate)' +
                                            ' and RecordType.Name in :recordTypeNames' +
                                            ' and T1C_Base__Account_Coverage__r.T1C_Base__Employee__r.' + CoverageApprovalManagerController.empApproverField + ' in :approverIds' +
                                            ' order by T1C_Base__Account_Coverage__r.T1C_Base__Employee__r.Name,' +
                                            ' T1C_Base__Account_Coverage__r.T1C_Base__Account__r.Name';

            System.debug(acaiQueryInner);
                                                                            // Maybe query AC and get children
            list<T1C_Base__Account_Coverage_Additional_Info__c> acais = (list<T1C_Base__Account_Coverage_Additional_Info__c>)Database.query(acaiQueryInner);
            
            map<Id, list<T1C_Base__Account_Coverage_Additional_Info__c>> addInfoMap = new map<Id, list<T1C_Base__Account_Coverage_Additional_Info__c>>();
            map<Id, String> acAcctNameMap = new map<Id, String>();
            map<Id, set<Id>> empAddInfoMap = new map<Id, set<Id>>();

            for(T1C_Base__Account_Coverage_Additional_Info__c acai : acais)
            {
                list<T1C_Base__Account_Coverage_Additional_Info__c> infs = addInfoMap.get(acai.T1C_Base__Account_Coverage__c);
                acAcctNameMap.put(acai.T1C_Base__Account_Coverage__c, acai.T1C_Base__Account_Coverage__r.T1C_Base__Account__r.Name);

                if(infs == null)
                {
                    infs = new list<T1C_Base__Account_Coverage_Additional_Info__c>();
                    addInfoMap.put(acai.T1C_Base__Account_Coverage__c, infs);
                }

                infs.add(acai);
                
                empDataMap.put(acai.T1C_Base__Account_Coverage__r.T1C_Base__Employee__c,null);

                set<Id> infos = empAddInfoMap.get(acai.T1C_Base__Account_Coverage__r.T1C_Base__Employee__c);

                if(infos == null)
                {
                    infos = new set<Id>();
                    empAddInfoMap.put(acai.T1C_Base__Account_Coverage__r.T1C_Base__Employee__c, infos);
                }

                infos.add(acai.T1C_Base__Account_Coverage__c);
            }

            set<Id> empDataMapKeySet = empDataMap.keySet();

            String empQuery = 'Select Id, Name, T1C_Base__Email__c, ';

            for(String field : empIdentityFields)
            {
                empQuery += field + ',';
            }

            empQuery = empQuery.substring(0, empQuery.length() -1);

            empQuery += ' from T1C_Base__Employee__c where Id in : empDataMapKeySet';

            for(T1C_Base__Employee__c emp : Database.query(empQuery))
            {
                empDataMap.put(emp.Id, emp);
            }

            String htmlBody = headerHTML + '<table border="0" cellpadding="0" cellspacing="0"  style="width: 100%;"><tr><td>' +openingLine + '</td></tr></table>';
            String textBody = openingLine;

            for(Id empId : empDataMap.keySet())
            {
                list<Coverage> covs = new list<Coverage>();
                T1C_Base__Employee__c emp = empDataMap.get(empId);                        
                set<Id> coverages = empAddInfoMap.get(empId);

                if(coverages != null)
                {
                    for(Id acId : coverages)
                    {
                        list<T1C_Base__Account_Coverage_Additional_Info__c> thisAcais = addInfoMap.get(acId);
                        String acctName = acAcctNameMap.get(acId);

                        if(thisAcais != null && acctName != null)
                        {
                            covs.add(new Coverage(acctName,thisAcais));
                        }
                    }
                }

                if(emp != null && !covs.isEmpty())
                {
                    String empNameAndTitle = emp.Name;
                    String identityString = '';
                    Boolean hasIdentyInfo = false;

                    if(!empIdentityFields.isEmpty())
                    {
                        identityString += '(';

                        for(String field : empIdentityFields)
                        {
                            if(emp.get(field) != null)
                            {
                                hasIdentyInfo = true;
                                identityString += String.valueOf(emp.get(field)) + ', ';
                            }
                        }

                        if(hasIdentyInfo)
                        {
                            identityString = identityString.substring(0, identityString.length() - 2);

                            identityString += ')';
                        }
                        else 
                        {
                            identityString = '';
                        }
                        
                    }

                    if(!String.isEmpty(identityString))
                    {
                        empNameAndTitle += ' ' + identityString;
                    }

                    textBody += empNameAndTitle + '\n' + emp.T1C_Base__Email__c + '\n';

                    htmlBody += spacerDiv;

                    htmlBody += '<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;"><tr>';

                    if(!String.isEmpty(imagePlaceHolderUrl))
                    {
                        htmlBody += '<td rowspan="2" style="width:35px"><img src="' + imagePlaceHolderUrl + '"/></td>';
                    }
                    
                    htmlBody += '<td style="' + empNameStyle + '">' + empNameAndTitle + '</td><tr><td><a style="' + empNameStyle + ';" href=mailto:' + emp.T1C_Base__Email__c + '>' + emp.T1C_Base__Email__c + '</a></td></tr></table>';

                    Integer coverageCount = 0;

                    for(Coverage cov : covs)
                    {
                        coverageCount++;
                        textBody += coverageCount + '. ' + cov.acctName + '\n';
                        
                        htmlBody += lineBreakDiv;

                        htmlBody += '<table border="0" cellpadding="0" cellspacing="0"  style="width: 100%;"><tr><td style="font-weight:800">' + coverageCount + '. ' + cov.acctName + '</td></tr>';

                        for(T1C_Base__Account_Coverage_Additional_Info__c inf : cov.addInfos)
                        {
                            String role = inf.T1C_Base__Role__c;
                            String region = inf.T1C_Base__Region__r.Name;
                            String product = inf.T1C_Base__Product__r.Name;

                            String covTextBody = '';
                            String covHTMLBody = '<tr><td style="padding-left:25px">';
                            Boolean hasCovInfo = false;

                            if(role != null)
                            {
                                hasCovInfo = true;
                                covTextBody += 'Role: ' + role + ' | ';
                                covHTMLBody += '<b>Role</b>: ' + role + ' | ';
                            }

                            if(region != null)
                            {
                                hasCovInfo = true;
                                covTextBody += 'Region: ' + region + ' | ';
                                covHTMLBody += '<b>Region</b>: ' + region + ' | ';
                            }

                            if(product != null)
                            {
                                hasCovInfo = true;
                                covTextBody += 'Product: ' + product + ' | ';
                                covHTMLBody += '<b>Product</b>: ' + product + ' | ';
                            }
                            
                            if(hasCovInfo)
                            {
                                covTextBody = covTextBody.substring(0, covTextBody.length() - 3);
                                covHTMLBody = covHTMLBody.substring(0, covHTMLBody.length() - 3);
                            }

                            covTextBody += '\n';
                            covHTMLBody += '</td></tr>';

                            textBody += covTextBody + '\n\n';
                            htmlBody += covHTMLBody;
                        }

                        htmlBody += '</table>';
                    }
                
                    htmlBody += lineBreakDiv;
                } 
            }

            htmlBody += '</table>';
            
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] {u.Id};
            message.subject = subject + u.Name;
            message.plainTextBody = textBody;
            htmlBody += footerHTML;            

            message.setHtmlBody(htmlBody);

            messages.add(message);
        }

        if(!messages.isEmpty())
        {
            try 
            {
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            } 
            catch (Exception e) 
            {
                system.debug('Cannot email right now, please check email settings');
            }           
        }
    }

    global void finish(Database.BatchableContext context)
	{

    }

    public Class Coverage
    {
        public String acctName;
        public list<T1C_Base__Account_Coverage_Additional_Info__c> addInfos;

        public Coverage(String acName, list<T1C_Base__Account_Coverage_Additional_Info__c> infs)
        {
            acctName = acName;
            addInfos = infs;
        }
    }
}