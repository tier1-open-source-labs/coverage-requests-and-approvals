
/**
 * Name: CoverageApprovalsTriggerHelper
 * Description: Helper class for the dupe logic for the coverage approvals form
 *
 * Confidential & Proprietary, 2015 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 */
public with sharing class CoverageApprovalsTriggerHelper 
{
    private static final String APPROVALS_CONFIG_PATH = 'ACE.Extensions.AccountCoverageApprovals.Settings';

    //The approver field. This is assumed to be on the Employee Object
    public static List<String> dupeQueryFields
    {
        set
        {

        }

        get
        {
            if(dupeQueryFields == null)
            {
                Set<String> tempFields = new Set<String>();
                T1C_FR.Feature coverageFeat = T1C_FR.FeatureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), APPROVALS_CONFIG_PATH + '.DuplicateRules');

                if(coverageFeat != null)
                {
                    for(T1C_FR.Feature subs : coverageFeat.subFeatures)
                    {
                        CoverageDupeRule newRule = new CoverageDupeRule(subs);
                        
                        tempFields.addAll(newRule.fieldsList);
                    }
                }
                
                tempFields.addAll(new String[]{'Id', 'T1C_Base__Role__c', 'T1C_Base__Account_Coverage__r.T1C_Base__Employee__r.Name',
                    'T1C_Base__Account_Coverage__r.T1C_Base__Employee__c', 'T1C_Base__Account_Coverage__r.T1C_Base__Account__c', 'RecordType.Name'});

                dupeQueryFields = new List<String>(tempFields);
            }

            return dupeQueryFields;
        }
    }

    //Check dupes similar to how the trigger does.
    //In this instance we can assume only one employee since the call is coming from the front end
    //This is for records that have not yet been inserted
    //The map is expected to have the grid row ids for easy transition back to the front end
    @RemoteAction
    public static List<String> checkDupes(Map<String, SObject> inCovsMap)
    {
        //Loop through and make a map using role product account and employee
        Map<String, String> empCoverageMap = new Map<String, String>();

        List<String> errorRecords = new List<String>();

        Map<String, CoverageDupeRule> roleToRuleMap = new Map<String, CoverageDupeRule>();
        Set<String> queryFields = new Set<String>();
        //Map out the different combinations in the AFRs
        T1C_FR.Feature coverageFeat = T1C_FR.FeatureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), APPROVALS_CONFIG_PATH + '.DuplicateRules');
        for(T1C_FR.Feature subs : coverageFeat.subFeatures)
        {
            CoverageDupeRule newRule = new CoverageDupeRule(subs);
            //Add each role to the map
            newRule.addRuleToMap(roleToRuleMap);
            //Need to sve the fields we're comparing against to query for the existing ACAIs
            queryFields.addAll(newRule.fieldsList);
        }

        //Get the record types that we need to check against
        String restrictedRecordTypes = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), 
        'ACE.Extensions.AccountCoverageApprovals.Settings', 'DupeCheckRecordTypes');
        List<String> restrictedRecordTypesList = restrictedRecordTypes.split(',');
        if(restrictedRecordTypesList.size() < 1)
        {
            restrictedRecordTypesList.add('Standard');
        }

        //The parent coverage will have the non role-restricted dupe checking
        CoverageDupeRule parentRule = new CoverageDupeRule(coverageFeat);
        roleToRuleMap.put('general', parentRule);
        queryFields.addAll(parentRule.fieldsList);

        //Since we have to do a comparison against existing records, we need to query
        //Gather all the Ids - both newly inserted acai and their account Ids
        Set<Id> acaiIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();

        for(String mapKey : inCovsMap.keySet())
        {
            SObject acai = inCovsMap.get(mapKey);
            String key = '';
            
            Id acntId = getFieldValue(acai, 'T1C_Base__Account_Coverage__r.T1C_Base__Account__c');

            accountIds.add(acntId);
            //The role should be made mandatory here
            if(acai.get('T1C_Base__Role__c') == null)
            {
                continue;
            }

            //TODO this can also be made configurable
            String curRole = (String)acai.get('T1C_Base__Role__c');
            CoverageDupeRule activeRule = roleToRuleMap.get(curRole);
            
            //Checking for restricted roles
            if(activeRule != null)
            {
                key = activeRule.generateKey(acai);
                if(key.length() > 0)
                {
                    if(empCoverageMap.get(key) == null)
                    {
                        empCoverageMap.put(key, mapKey);
                    }else
                    {
                        errorRecords.add(mapKey + ':' + activeRule.errorMessage);
                    }
                }
            }
            //This is for employee dupes
            CoverageDupeRule generalRule = roleToRuleMap.get('general');
            String empKey = generalRule.generateKey(acai);
            system.debug('Found emp Key for General: ' + empKey);

            if(empKey.length() > 0)
            {
                
                if(empCoverageMap.get(empKey) == null)
                {
                    empCoverageMap.put(empKey, mapKey);
                }else
                {
                    errorRecords.add( empCoverageMap.get(empKey)  + ':' + generalRule.errorMessage );
                    errorRecords.add( mapKey + ':' + generalRule.errorMessage );
                }
            }
            
            //Check for dupes
        }

        //Now we query for the existing ACAIs

        /*
        [SELECT Id, 
                T1C_Base__Region__c,
                T1C_Base__Product__c,
                T1C_Base__Role__c,
                T1C_Base__Account_Coverage__r.T1C_Base__Employee__c,
                T1C_Base__Account_Coverage__r.T1C_Base__Account__c
            FROM T1C_Base__Account_Coverage_Additional_Info__c
            WHERE T1C_Base__Account_Coverage__r.T1C_Base__Account__c IN :accountIds
            AND T1C_Base__Inactive__c != 'true'
        LIMIT 10000]
        
        */
        //Default fields that are required for the logic to work
        queryFields.addAll(new String[]{'Id', 'T1C_Base__Role__c', 'T1C_Base__Account_Coverage__r.T1C_Base__Employee__r.Name',
            'T1C_Base__Account_Coverage__r.T1C_Base__Employee__c', 'T1C_Base__Account_Coverage__r.T1C_Base__Account__c', 'RecordType.Name'});

        Set<String> inputIds = inCovsMap.keySet();

        String query = 'SELECT ' + String.join(new List<String>(queryFields), ',') + 
                       ' FROM T1C_Base__Account_Coverage_Additional_Info__c ' +
                       ' WHERE T1C_Base__Account_Coverage__r.T1C_Base__Account__c IN :accountIds' + 
                       ' AND RecordType.Name IN :restrictedRecordTypesList ' +
                       ' AND Id NOT IN :inputIds ' + 
                       ' AND (T1C_Base__Inactive__c != \'true\' OR Approval_Status__c = \'Pending\')' ;

        for(T1C_Base__Account_Coverage_Additional_Info__c existingAcai : Database.query(query))
        {
            
            String curkey = '';
            String curRole = existingAcai.T1C_Base__Role__c;

            CoverageDupeRule activeRule = roleToRuleMap.get(curRole);
            
            //Checking for restricted roles
            if(activeRule != null)
            {
                curkey = activeRule.generateKey(existingAcai);
                System.debug('Found Key: ' + curkey);
                if(curkey.length() > 0)
                {
                    if(empCoverageMap.get(curkey) != null)
                    {
                        errorRecords.add( empCoverageMap.get(curkey)  + ':' + activeRule.errorMessage +
                        ':' + existingAcai.T1C_Base__Account_Coverage__r.T1C_Base__Employee__r.Name );
                    }
                }
            }

            CoverageDupeRule generalRule = roleToRuleMap.get('general');
            String empKey = generalRule.generateKey(existingAcai);
            system.debug('Found emp Key for existing General: ' + empKey);
            if(empKey.length() > 0)
            {
                //If this is null it means that we aren't submitting anything that would be a dupe
                if(empCoverageMap.get(empKey) == null)
                {
                    continue;
                }else
                {
                    errorRecords.add( empCoverageMap.get(empKey) + ':' + generalRule.errorMessage + 
                        ':' + existingAcai.T1C_Base__Account_Coverage__r.T1C_Base__Employee__r.Name );
                }
            }
        }

        return errorRecords;
    }

    public static String getFieldValue(SObject inObj, String propertyName)
    {
        List<String> propsList = propertyName.split('\\.');
        
        //Remove the last item and use it as the get after the loop
        String finalProperty = propsList.size() > 0 ? propsList.remove( propsList.size() - 1 ) : propertyName;
        
        //For record type, in order to save on a query we'll use schema instead
        //This is done because the record type is not actually saved, but rather the recordtypeid
        if(propertyName.contains('RecordType'))
        {
            Map<Id, Schema.RecordTypeInfo> infos = Schema.SObjectType.Account.getRecordTypeInfosById();
            Id recordTypeId = (Id)inObj.get('RecordTypeId');
            String recordTypeName = infos.get( recordTypeId ).getName();
            return recordTypeName;
        }

        if(propsList.size() > 0)
        {
            for(String curProp : propsList)
            {
                //curProp = curProp.replace('__r', '__c' );
                inObj = inObj.getSObject(curProp);
            }
        }
        
        try{
            return inObj.get(finalProperty) != null ? String.valueOf( inObj.get(finalProperty) ) : '';
        }catch(Exception e)
        {
            System.debug('Exception when checking for Dupes!' + e.getMessage());
            return '';
        }
        
    }

    //Class to represent the dupe rules in the AFRs
    private class CoverageDupeRule
    {
        public String[] fieldsList;
        public String[] restrictedRoles;
        public String errorMessage;

        //Instantiate using the attribute values
        //Having it setup like this means that Employee ID and Account ID now have to be manually added as well
        public CoverageDupeRule(T1C_FR.Feature inFeat)
        {
            this.fieldsList = inFeat.getAttributeValue('MatchFields', '').split(',');
            this.restrictedRoles = inFeat.getAttributeValue('RestrictedRoles', '').split(',');
            this.errorMessage = inFeat.getAttributeValue('Message', '');
        }

        public void addRuleToMap(Map<String, CoverageDupeRule> inMap)
        {
            for(String str : this.restrictedRoles)
            {
                inMap.put(str, this);
            }
        }

        //Generates a unique key in order to compare dupes
        //Based on the fields List
        //This is so that we know which combinations have already been taken
        public String generateKey(SObject anyObj)
        {
            String genKey = '';
            for(String field : this.fieldsList)
            {
                
                genKey += CoverageApprovalsTriggerHelper.getFieldValue(anyObj, field);
            }
            return genKey;
        }

    }

}
