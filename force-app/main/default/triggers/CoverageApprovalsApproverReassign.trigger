/**
 * Name: CoverageApprovalManagerController
 * Description: A trigger that handles the resubmission of pending ACAI records to 
 *              the new approver when an Employee's Approver has been re-assigned
 *
 * Confidential & Proprietary, 2020 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 */
trigger CoverageApprovalsApproverReassign on T1C_Base__Employee__c (after update)
{
    //Tier1 trigger check
    if(T1C_Base.ACETriggerCheck.isDisabled('CoverageApprovalsApproverReassign'))
    {
    	return;
    }

    Map<Id, Id> submitterIdsMap = new Map<Id, Id>();
    //Gather all of the Old Approver User Ids
    for(T1C_Base__Employee__c curEmp : Trigger.old)
    {
        T1C_Base__Employee__c newEmp = Trigger.newMap.get(curEmp.Id);
        String newApproverId = CoverageApprovalsTriggerHelper.getFieldValue(newEmp, CoverageApprovalManagerController.empApproverField);
        String oldApproverId = CoverageApprovalsTriggerHelper.getFieldValue(curEmp, CoverageApprovalManagerController.empApproverField);

        if(oldApproverId != newApproverId && newApproverId.length() > 0)
        {
            submitterIdsMap.put(curEmp.T1C_Base__User__c, (Id) newApproverId);
        }
    }

    List<Approval.ProcessSubmitRequest> reassignedList = new List<Approval.ProcessSubmitRequest>();

    List<Approval.ProcessWorkitemRequest> removedList = new List<Approval.ProcessWorkitemRequest>();

    //We use this map to scan for valid objects since the process instance object can have any object in the TargetObjectId
    Map<String, CoverageApprovalInterfaces.CoverageInserter> validTypesMap = CoverageApprovalManagerController.getSOBjectTypesMap();

    List<SObject> coverages = new List<SObject>();

    List<Id> approverIds = new List<Id>();

    for(ProcessInstance curInst : [SELECT Id,
                                          TargetObjectId,
                                          TargetObject.Type,
                                          Status,
                                          SubmittedById,
                                          (SELECT Id, ActorId, ProcessInstanceId FROM Workitems)
                                     FROM ProcessInstance
                                    WHERE SubmittedById IN :submitterIdsMap.keySet()])
    {

        String objType = curInst.TargetObjectId.getSobjectType().getDescribe().getName();
        System.debug('Found Type for re-assignment: ' + objType);

        if(curInst.Workitems.size() < 1 || validTypesMap.get(objType) == null)
        {
            continue;
        }
        //Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        
        req.setObjectId( curInst.TargetObjectId );
        req.setSubmitterId( curInst.SubmittedById );
        req.setNextApproverIds( new List<Id>{submitterIdsMap.get(curInst.SubmittedById)} );
        reassignedList.add(req);

        approverIds.add(submitterIdsMap.get(curInst.SubmittedById));

        //Cancel the old request
        Approval.ProcessWorkitemRequest oldReq = new Approval.ProcessWorkitemRequest();
        oldReq.setAction('Removed');
        oldReq.setWorkitemId(curInst.Workitems[0].Id);

        removedList.add(oldReq);

        SObject obj = curInst.TargetObjectId.getSobjectType().newSObject(curInst.TargetObjectId);

        coverages.add(obj);
    }

    if(reassignedList.size() > 0)
    {

        List<Approval.ProcessResult> removed = Approval.process(removedList);
        List<Approval.ProcessResult> result = Approval.process(reassignedList);

        CoverageApprovalManagerController.sendEmailAggregateToApprover(
            coverages, approverIds, CoverageApprovalManagerController.newCovsEmailSubjectLine, CoverageApprovalManagerController.newCovsEmailOpenLine);
    }

}