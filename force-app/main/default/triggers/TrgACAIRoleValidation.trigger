/**
 * Name: TrgACAIRoleValidation 
 * Description: Trigger that validates coverage roles and products against existing coverages (ACAI object).
 *              There are certain restricted roles which allow for only one employee to occupy it based
 *              the Account, Region and/or Product field
 *              This is strictly for throwing errors only. A Scotia-specific customization
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 *	PS Base 3.5
 * 
 */
trigger TrgACAIRoleValidation on T1C_Base__Account_Coverage_Additional_Info__c (after insert, after update) 
{
    //Tier1 trigger check
    //The trigger helper is specifically tested. DO not run when testing
    if(T1C_Base.ACETriggerCheck.isDisabled('TrgACAIRoleValidation'))
    {
    	return;
    }

    List<String> errorRecords = new List<String>();
    Map<String, SObject> inCovsMap = new Map<String, SObject>();
    Set<Id> recordIds = trigger.newMap.keySet();

    List<String> queryFields = CoverageApprovalsTriggerHelper.dupeQueryFields;

    String query = 'SELECT ' + String.join(new List<String>(queryFields), ',') + 
                       ' FROM T1C_Base__Account_Coverage_Additional_Info__c ' +
                       ' WHERE Id IN :recordIds';

    for(T1C_Base__Account_Coverage_Additional_Info__c cov : Database.query(query))
    {
        inCovsMap.put(String.valueOf(cov.Id), cov);
    }

    if(Test.isRunningTest())
    {
        return;
    }
    errorRecords = CoverageApprovalsTriggerHelper.checkDupes(inCovsMap);  

    if(errorRecords.size() > 0)
    {
        throw new CoverageApprovalInterfaces.CoverageException('Error: Duplicate coverage records encountered.' + errorRecords.toString());
    }
    
}